---
title: About me
subtitle: It is what it is.
comments: false
---

# Hi there I am Edzo. 

Welcome to my **About page**.  

{{< table_of_contents >}}


## Who am I
Since 1991 I am in ITm, because
at age 10 my teachers advised used a typewriter 
and this quickly became a computer. 
A classmate and I created a virtual software company
at age 11, etc. Game was on. 
At 12 I founded a computer club during my first year at high-school, 
at 14 a computer help-desk, at 15 won prizes for combining IT in English education.
In this period I also created various commercial websites 
and helped companies with their IT issues. 
Founded my real company at age 18, and started
with computer science at the university of Groningen. 
During my university years I obviously continued my activities
in various boards, committees etc. 
Expanding my experience in collaboration, organising and governing. 
In 2006 I started with full time consultancy and helped 
30 companies in over 40 assignments.

### My view
I think Technology including IT should support people.
The fundamentals of IT and of most things are pretty easy. 
This leads me to the conviction that all crazy problems
are created by ourselves. 
It only takes a bit of experience and education to identify the fundamentals
in all the fluff we created.


### My speciality
I excel and feel at home in international and complex environments.
Usually I take the role of Architect for the topic of 
the whole of the Enterprise, the scope of a Project Program, 
the Security domain with as specialisms antifragility, resilience and variety.

### Where do I thrive
Due to my hunger for knowledge, and extensive experience in 
business and IT, most problems I can decompose and fix within
a few days with a max of 14 days. This also accounts for 
large international scopes. Drop me a line if you get stuck 
with large international multi project programs. I love to help. 
Since there is no need to get stuck for years, it it is better
to have fun creating stuff and getting new products out to the public.

### Knowledge sharing
I am convinced that knowledge has no owner. Nobody should be entitled
to have the soul access to certain knowledge, since we all are just
temporary here on this planet and do not know anything without the help,
experience and knowledge of others. 


<!-- blank line -->
----
<!-- blank line -->
## Badges

- Experienced Speaker
- Experienced Writer
- Experienced Coach
- Experienced Problem Solver (Business and IT)

<!-- blank line -->
----
<!-- blank line -->
## Timeline
- since 1991 in IT 
- since 1993 IT experience via helping companies and people
- since 1995 organisational experience via founding organisations
	- student helpdesk to help teachers
	- student computer club
	- first in digital educational content
- since 1999 business experience via founding own company and volunteering
- since 1999 writer of blogs and articles.
- since 2006 in many (international) talent programs
- since 2006 30 clients / 40 assignments
- since 2017 Sogetilabs Fellow
- since 2017 Architecture Thought-leadership team
- in 2020 MSc graudated summa cum laude at Antwerp Management School
- since 2021 active as security consultant
- in 2021 started PhD in Resilience and Information Security at Open University

[doi-thesis]: https://doi.org/10.5281/zenodo.3719388
[git-thesis]: https://gitlab.com/edzob/antifragile-research
[git-article]: https://gitlab.com/edzob/antifragile-article-eaal
[SL-blogger]: https://blog.edzob.com/2019/01/sogeti-labs-posts.html
[AG-blogger]: https://blog.edzob.com/2020/08/ag-connect-blog-posts-dutch.html


<!-- blank line -->
----
<!-- blank line -->

## My personality

### My values
- Human Centred Design
- Equality, Liberty and Fraternity
- Radical Open Everything
- Privacy and Security are important to guarantee the above.

### My Myers-Briggs Personality Type
- MBTI Personality types: 
[ENFP-T](https://www.16personalities.com/profiles/38f832ef73f64) 
(and [ENTJ](https://en.wikipedia.org/wiki/Fieldmarshal_(role_variant)) is my back seat driver).

### My favourite quotes

> "It is amazing what you can accomplish if you do not care who gets the credit."    
> - Harry S Truman

> “Clear Eyes, Full Hearts, Can't Lose.”     
> - Coach Taylor

> "I hear and I forget. I see and I remember. I do and I understand."     
> - Confucius

> Reality is a Complex Adaptive System-of-Systems 
> showing a non-linear behaviour 
> - (Dietz et al., 2013, p. 93)


### My motto
I try not to forget that we are all observing reality through our own personal lens. 
We need to be cautious not to assume that what we see is the same as what someone else observes.

<!-- blank line -->
----
<!-- blank line -->

## How I work

### 1. Human Centred
- Get to know the people.
- Get to know the language of the organisation.

### 2. Goal and Complex System oriented 
- Find the problem and how to validate if the problem is solved.
- Approach the systems from a Complex Systems holistic viewpoint.
- I use Enterprise Engineering as a framework for my professional mental model. 

### 3. Communication is key 
- I believe that the alignment of communication and emotion that bring success to an organisation.

### 4. IT has no secrets for me
- My roots are (deep) in the IT. 
- My understanding is that IT is just a tool.
- For me most IT issues are not IT issues but organisation alignment issues, 
  therefore I try to avoid the distinction between business and IT.
- Inspiration and collaboration are the key to valuable usage of any tool.
