---
title: Groningen Notes
subtitle: fieldnotes for wifi and coffee
comments: false
tags: ["Groningen", "Coffee", "Wifi"]
---

<!-- https://www.markdownguide.org/basic-syntax/#horizontal-rules -->
<!-- https://github.com/liwenyip/hugo-easy-gallery -->

# Flexwork 
__Requirements__ - quiet, no echo, wifi, powersocket. Room to walk into for a quiet/private video-call, around 4-9 hours sitting

1. [Student Hotel Groningen][shg-wp] (Boterdiep 9, 9712 LH Groningen) - 15E per day (?)
1. [Launch Cafe Groningen][lcg] (Herestraat 106, 9711LM Groningen) - 30E per day (?)

[archive]: https://www.groningerarchieven.nl/
[shg-wp]: https://www.thestudenthotel.com/co-working-space/
[lcg]: https://www.launchcafe.nl/en/flexible-workspace-groningen/

{{< gallery >}}
{{< figure link="https://www.thestudenthotel.com/497786/globalassets/03.-property-pages/groningen/00-images/01-landing-page/02-image-gallery/study-room/the_student_hotel_groningen_study_area_book_shelf_full_of_books_and_blue_pillar.jpg?w=480&h=270&mode=max&scale=both&quality=90" caption="Work at Student Hotel Groningen" >}}
{{< figure link="https://www.launchcafe.nl/wp-content/uploads/2018/10/DSC_0340-1-1400x933.jpg" caption="Work at Launch Cafe Groningen" >}}
{{< /gallery >}}

# Work with inspiration
__Requirements__ - good coffee, nice people, good wifi, table with a view. People walking around, around 2 hours sitting
1. [Forum Groningen][forum] 
    (Nieuwe Markt, 19712 KJ Groningen)
1. [DOT Gronignen][dot] 
    (Vrydemalaan 2, 9713 WS Groningen)
1. [Student Hotel Groningen][shg] 
    (Boterdiep 9, 9712 LH Groningen)
1. [University Library Groningen][ulg] 
    (Broerstraat 4, 9712 CP Groningen) 
    free for NL students (incl OU), 
    [48,- or 78,-][ulgpass] per year
1. [De Kater werkplekken][kater-werk] 
    (Gedempte Kattendiep 33, 9711 Pm Groningen)

{{< gallery >}}
{{< figure link="https://forum.nl/cache/fiction.5158/fiction-s1024x1024-q60.jpg" caption="Forum Groningen" >}}
{{< figure link="https://discovergroningen.com/wp-content/uploads/2018/03/DOT_Groningen_2-1600x1060.jpg"  caption="Dot Groningen" >}}
{{< figure link="https://www.thestudenthotel.com/4973af/globalassets/03.-property-pages/groningen/00-images/04-meet/02-experience/small/the_student_hotel_groningen_the_commons_colourful_area.jpg?w=767&h=597&mode=max&scale=both&quality=90" caption="Student Hotel Groningen" >}}
{{< figure link="https://pbs.twimg.com/media/EUmOtV3WAAA-aZP.jpg" caption="University Library" >}}
{{< figure link="https://lh3.googleusercontent.com/p/AF1QipNGndk3Z4ml8BUsg2p9SnNNQQnwCNmmuav8y9dk=w960-h960-n-o-v1" caption="Kater" >}}
{{< /gallery >}}


[forum]: https://forum.nl/nl/studeren-in-het-forum
[dot]: https://www.dotgroningen.nl/#menu
[shg]: https://www.thestudenthotel.com/groningen/
[ulg]: https://www.rug.nl/library/support/faq?tcid=verint_1071_15_47
[ulgpass]: https://www.rug.nl/library/_shared/pdf/pdf-access-librarycard-eng.pdf
[kater-werk]: https://www.dekatergroningen.nl/werkplekken/

## Work with inspiration - in a café
1. [The Commons Restaurant Groningen][commons] (Boterdiep 11, 9712 LH Groningen) (previously known as "the pool")

{{< gallery >}}
{{< figure link="https://media-cdn.tripadvisor.com/media/photo-s/1b/1e/f9/0b/the-commons-groningen.jpg" caption="The Commons" >}}
{{< /gallery >}}


[commons]: https://www.thecommonsrestaurant.com/groningen
[uur]: https://uurwerker.nl
[cc]: https://coffeecompany.nl/locations/oude-ebbingestraat-39
[kater]: https://www.dekatergroningen.nl
[concert]: http://www.hetconcerthuis.nl
[sal]: http://salmagundi-living.nl/lunchtime
[flinders]: http://www.flinderscafe.nl/groningen
[lab050]: https://www.labnul50.nl
[ks]: https://koffiestation.nl
[spaak]: https://spaak.cc/
[tusc]: https://www.tucanocoffee.com/en/locations/netherlands-en
[sleutel]: https://www.cafedesleutel.nl/
[foodz]: https://www.foodmatterz.nl/contact
[sigaar]: https://www.cafedesigaar.nl/

# Reading with chill
__Requirements__ - good coffee, powersocket, nice people, comfy chairs, chairs with a view, quiet, no echo

### cafés to be reviewed
1. [De Uurwerker][uur] 
    (Uurwerkersplein 1, 9712 EJ Groningen)
1. [Coffee Company Oude Ebbinge Groningen][cc] 
    (Oude Ebbingestraat 39 ,9712 HB Groningen)
1. [De kater][kater]
    (Gedempte Kattendiep 33, 9711 Pm Groningen)
1. [Het Concerthuis][concert] 
    (Poelestraat 30, 9712 KB Groningen)
1. [Salmagundi’s][sal]
    (J.C. Kapteynlaan 6-8, 9714 CP Groningen)
1. [Flinders Café][flinders]
    (Schuitendiep 54, 9711 TD Groningen)
1. [Lab050 Brasserie][lab050]
    (Boterdiep 113, 9712 LM Groningen) (aka Het Paleis)
1. [Koffiestation][ks] 
    (Turfsingel 4, 9712 KP Groningen)
1. [Spaak Koffie en Koers][spaak]
    (Oude Boteringestraat 66, 9712 GN Groningen)
1. [Tucano Coffee][tusc]
    (Oude Kijk in Het Jatstraat 52, 9712 EL Groningen)
1. Bartista
1. Wadapartja
1. Black & Bloom (Great coffee, perhaps not place to sit down)
1. Feel Good
1. Bakkerij Blanche 
1. Pernikkel
1. [Café De Sigaar][sigaar]
    (Hoge der A 2, 9712 AC Groningen)
1. [Café de Sleutel][sleutel]
    (Noorderhaven 72, 9712 VM Groningen)

#### No Laptop Places
1. [Food Matterz][foodz] - No Laptops!
    (Oude Ebbingestraat 86, 9712 HM Groningen)

### Hotel restaurants to be reviews
[simplon]: https://simplonhostel.nl/
[bwg]: https://hotelgroningencentre.nl
[mh]: https://www.martinihotel.nl
[ah]: https://www.asgardhotel.nl/neem-contact-op/
[chg]: https://www.cityhotelgroningen.com/en/

1. [Best Western Hotel Groningen Centre][bwg]
    (Radesingel 50, 9711 EK Groningen)
1. [Simplon Hostel][simplon]
    (Boterdiep 73-2, 9712 LL Groningen)
1. [Het Martini Hotel][mh]
    (Gedempte Zuiderdiep 8, 9711 HG GRONINGEN)
1. [Asgardhotel][ah]
    (Ganzevoortsingel 2-1, 9711 AL Groningen)
1. [City Hotel Groningen][chg]
    (Gedempte Kattendiep 25, 9711 PM Groningen)



# Backup plan
Locations for long time remote work

1. https://www.bewaaktenbewoond.nl/werkruimte/
1. Kantoor Groningen / https://kantoor-groningen.nl/
1. Werkplek050 / https://werkplek050.nl/werkplek/ /  Hereplein 4, 9711 GA Groningen / 80E pm

---

# Resources

These are (old) sites that provide lists. 
These lists where input for the curated list.
1. https://flexwerkplek.nl/steden/groningen/
    1. Bartista
    bartista.nl / Ubbo Emmiusstraat 34a
    1. Wadapartja
    wadapartja.nl/#zuiderdiep / Gedempte Zuiderdiep 39-41
    . Doppio Groningen
    / Brugstraat 6
    1. Forum Groningen
    https://forum.nl/nl/studeren-in-het-forum / Nieuwe Markt 1
    1. Het Heerenhuis
    www.hetheerenhuis.com / Spilsluizen 9
    1. Black & Bloom
    blackandbloom.nl / Oude Kijk in ‘t Jatstraat 32
    1. Tucano Coffee
    tucanocoffee.com/en / Oude Kijk in Het Jatstraat 52
    1. The Student Hotel
    www.thestudenthotel.com / Boterdiep 9
    1. Feel Good
    www.feelgoodgroningen.nl / Oude Kijk in 't Jatstraat 29
    1. De Uurwerker
    uurwerker.nl /Uurwerkersplein 1
1. https://zzpbarometer.nl/2021/05/18/zzper-beste-flexwerkplekken-groningen/
    1. businessenzo / https://businessenzo.nl/
    1. regus / https://www.regus.com/nl-nl/netherlands/groningen/paterswoldseweg-806-2837
    1. launchcafe / https://www.launchcafe.nl/
    1. Kantoor Groningen / https://kantoor-groningen.nl/
    1. Paleis groningen / https://www.hetpaleisgroningen.nl/
    1. Mediacentrale / https://mediacentrale.com/mediacafe
    1. de chemie / https://www.dechemie.nl/
    1. de pijp / https://www.depijp-groningen.nl/
1. https://dvhn.nl/aangeboden/Dit-zijn-de-beste-flexwerkplekken-in-Groningen-26482363.html
    1. De Kranepoort
    1. HNK
    1. De Chemie
    1. Seats2Meet
    1. Launch Cafe  / https://www.launchcafe.nl/en/the-best-coworking-space-groningen/
1. https://gratisflexplek.nl/region/groningen/
    1. The Commons / Boterdiep 9, 9712 LH Groningen
        1. https://www.thecommonsrestaurant.com/wp-content/uploads/2020/06/All-Day-Menu_The-Commons.pdf
    1. Luchtstroom / Suikerlaan 3, 9743 DA Groningen
    1. Flinders Café / Nieuweweg 2, 9711 TD Groningen
    1. Bakkerij Blanche / Brugstraat 28, 9711 HZ Groningen
    1. Pernikkel / Aweg 2, 9718 CS Groningen
    1. De Uurwerker / Uurwerkersplein 1, 9712 EJ Groningen
        1. https://uurwerker.nl/wp-content/uploads/2020/10/Uurwerker-10-food-en-drinks.pdf
    1. Doppio Espresso / Brugstraat 6, 9711 HX Groningen
    1. Feel Good / Oude Kijk in Het Jatstraat 29, 9712 EB Groningen
1. https://www.xluitzendbureau.nl/blog/inspiratie-koffie-de-beste-werkplekken-in-groningen/
    1. CHCO Café Groningen / Oude Ebbingestraat 74, 9712 HM Groningen
    1. Black & Bloom / Oude Kijk in Het Jatstraat 32, 9712 EK Groningen
    1. Bakkerij Blanche / Brugstraat 28, 9711 HZ Groningen
    1. Doppio / Brugstraat 6, 9711 HX Groningen
    1. Seats2meet / Eemsgolaan 9, 9727 DW Groningen
    1. Martini / Paterswolderseweg 806, 9728BM Groningen
1. https://www.deskbookers.com/nl-nl/c/groningen
    1. 'Groninger Loft' /  Sint Jansstraat 2k, 9712JN Groningen
    1. Regus Groningen Martini / Paterswoldseweg 806, 9728BM Groningen
    1. LIVE-AED / Moermanskweg 2-10, 9723HM Groningen
    1. Het Coachhuis /  Leonard Springerlaan 36, 9727KB Groningen
1. https://groningen.nl/werk/een-bedrijf-opstarten/flexplekken-en-het-huren-van-werkruimte
    1. Het Launch Cafe
    1. De Pijp
    1. Het Paleis
    1. Mediacentrale
    1. R&D Hotel op Zernike (voor ondernemers gericht op energie, chemie, life science, big data en healthy ageing)
    1. Hashtag Workmode is een werkplaats (zowel flex als vast) voor uitsluitend vrouwen
    1. Plantoor
    1. Luchtstroom
    1. Shared Spaces Haren
    1. Business & Zo
    1. Werkplek050     

## Uitzoeken
1. https://kantoor-groningen.nl/locatie/puddingfabriek
1. https://indebuurt.nl/groningen/eten-drinken/dakterras-groningen-uitzicht-leukste-dakterrassen~124816/5/
1. https://www.sikkom.nl/zeven-chille-dakterrassen-in-groningen/
1. https://magnet.me/blog/nl/de-6-beste-studieplekken-in-groningen/
1. https://www.lasanne.nl/groningen/de-leukste-hotspots-groningen/
1. https://www.workjuice.nl/beste-werkplekken-groningen/
1. https://www.sikkom.nl/tien-koffietentjes-groningen/
1. https://www.koffietje.nl/plaats/groningen/
1. https://www.mapofjoy.nl/food-hotspots-groningen/
1. https://discovergroningen.com/de-leukste-koffiespots-in-groningen/
1. https://inspiratiecafe-oso.nl/flexwerkplek-groningen



### office
1. https://www.snelkantoor.nl/spaces/
1. https://www.postadresnederland.org/diensten
1. https://www.prioffice.eu/en/information/what-is-a-virtual-office/







