---
title: Contact me
subtitle: Signal by default
comments: false
---

#  My Contact information

Welcome to my **Contact page**.  


| Site                      |              
| -------------             |            
|[Homepage][Site]           |   
|[ORC ID][ORCid]            | 
|edzob@[LinkedIn][LinkedIn] | 

__Note__: For my email and phone number, see [My Resume](/page/resume/).


| Secure Messaging              
| -------------             
| edzob@Signal      
| edzob@[Matrix][Matrix]
| edzob@[Wire][Wire]   


[site]: https://www.edzob.com
[ORCid]: https://orcid.org/0000-0003-0097-7375
[LinkedIn]: https://www.linkedin.com/in/edzob

[Matrix]: @edzob:matrix.org
[Wire]: @edzob:app.wire.com