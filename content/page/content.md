---
title: My Content
subtitle: Micro-Blogs, Blogs and publications
comments: false
---

Welcome to my **Content page**.  

{{< table_of_contents >}}


# My Content
My content creation has evolved into the following pipeline.
My ideation process is pretty diverse, but usually topics
slowly evolve into blogs and then slowly get deepened into
white-papers which in itself are the foundation for
scientific research leading to scientific publications.

It is more a funnel then a pipeline. 

## Content creation funnel

### Books, Micro-Blogs, Blogs, Articles
| Books read   
| -------------              
| [Goodreads][Good]   
---
| Micro-Blogs           | Blogs / Articles / Interviews
| -------------         | -------------
| [Twitter][Twitter]    | [blogs EN][Blogger] @ [edzob][Blogger]
| [Mastodon][Mastodon]  | [blogs EN][Medium] @ [Medium][Medium]
|                       | [blogs EN][Xebia] @ [Xebia][Xebia]
|                       | [blogs NL][SL-blogger] @ [SogetiLabs][SogetiLabs]
|                       | [publications NL][AG-blogger] @ [AGConnect][AGConnect]
|                       | [publication NL][pvib-pdf] @ [PVIB][pvib]
|                       | [publication FR](https://www.ictjournal.ch/interviews/2024-03-04/edzo-botjes-lefficacite-soppose-en-quelque-sorte-a-la-resilience) @ [ICT Journal Swiss](https://www.ictjournal.ch/)
|                       | [publication NL](https://www.blogit.nl/brace-brace/) @ [BlogIT](https://www.blogit.nl)


[Twitter]: https://twitter.com/edzob
[Mastodon]: @edzob@hostux.social
[Good]: https://www.goodreads.com/review/list/19214230-edzo?shelf=to-read
[Blogger]: https://blog.edzob.com
[Medium]: https://medium.com/@edzob
[SogetiLabs]: https://labs.sogeti.com/experts/edzo-botjes
[AGConnect]:https://www.agconnect.nl/auteur/edzo-botjes
[Xebia]: https://xebia.com/author/ebotjes/

### White-papers
---
| White-Papers     |                |
| -------------    | -------------  | -------------
| [Architecture in this new world we live in][dya:whitepaper:sensemaking] | 2019&nbsp;11&nbsp;11 |  [pdf][dya:whitepaper:sensemaking:pdf-2] ([pdf-old][dya:whitepaper:sensemaking:pdf])
| [Value sensitive architecture][dya:whitepaper:valuesensitive]           | 2020&nbsp;09&nbsp;04 |  [pdf][dya:whitepaper:valuesensitive:pdf-2] ([pdf-old][dya:whitepaper:valuesensitive:pdf])
| [Design for Chaos][dya:whiteapaper:chaos]                       | 2021&nbsp;09&nbsp;22 | [pdf][dya:whiteapaper:chaos:pdf-2]  ([pdf-old][dya:whiteapaper:chaos:pdf] )      
| [Situational Architecturing][dya:whitepaper:situational] | 2022&nbsp;02&nbsp;23 | [pdf][dya:whitepaper:situational:pdf-2] ([pdf-old][dya:whitepaper:situational:pdf])

[dya:whitepaper:sensemaking]: https://labs.sogeti.com/architecture-in-this-new.world-we-live-in-a-dya-whitepaper-by-sogeti
[dya:whitepaper:valuesensitive]: https://labs.sogeti.com/whitepaper-value-sensitive-architecture
[dya:whiteapaper:chaos]: https://labs.sogeti.com/design-for-chaos-a-dya-whitepaper-by-sogeti
[dya:whitepaper:situational]: https://labs.sogeti.com/situational-architecturing-a-dya-whitepaper-by-sogeti/

[dya:whitepaper:sensemaking:pdf]: https://e476rzxxeua.exactdn.com/wp-content/uploads/2019/12/Architecture-in-this-new-world-we-live-in-a-DYA-white-paper-by-Sogeti-version-20191223-v2.pdf
[dya:whitepaper:valuesensitive:pdf]: https://e476rzxxeua.exactdn.com/wp-content/uploads/2020/09/Value-sensitive-architecture-20200902-v2.pdf
[dya:whiteapaper:chaos:pdf]: https://e476rzxxeua.exactdn.com/wp-content/uploads/2021/11/Design-for-Chaos-a-DYA-white-paper-by-Sogeti-version-20211008-v1.pdf
[dya:whitepaper:situational:pdf]: https://e476rzxxeua.exactdn.com/wp-content/uploads/2022/02/DYA-Situational-Architecting-An-integrated-governance-pattern-for-sensemaking-architecture.pdf


[dya:whitepaper:sensemaking:pdf-2]: https://e476rzxxeua.exactdn.com/wp-content/uploads/2022/04/DYA-whitepaper-Architecture-In-This-New-World-We-Live-In.pdf
[dya:whitepaper:valuesensitive:pdf-2]: https://e476rzxxeua.exactdn.com/wp-content/uploads/2022/04/DYA-whitepaper-Value-Sensitive-Architecture.pdf
[dya:whiteapaper:chaos:pdf-2]: https://e476rzxxeua.exactdn.com/wp-content/uploads/2022/04/DYA-whitepaper-Design-For-Chaos.pdf
[dya:whitepaper:situational:pdf-2]: https://e476rzxxeua.exactdn.com/wp-content/uploads/2022/02/DYA-Situational-Architecting-An-integrated-governance-pattern-for-sensemaking-architecture.pdf

### Essays/ Books
| Essays/ Bookes   |                |
| -------------    | -------------  | -------------
| [Survival of the fitting - An essay on Sensemaking Architecture](https://zenodo.org/records/10041096) | 2023&nbsp;05&nbsp;12 | [pdf](https://zenodo.org/records/10041096)

### Academic Work

---
| **Academic Work**                                                               |
| -------------                                                                   | ------------- 
| [Defining Antifragility and the application on Organisation Design][doi-thesis] | 2020&nbsp;05&nbsp;20 
| ○ MSc Thesis @ Antwerp Management School                                          |
| ○ LaTeX code of thesis [@Gitlab][git-thesis]                                                     | 
| [Attributes relevant to antifragile organizations][IEEE] | 2021&nbsp;11&nbsp;19
| ○ IEEE paper @ CBI 2021                                    |                    
| ○ LaTeX paper of paper [@Gitlab][git-article]                              | 
| NST&nbsp;EEWC&nbsp;Paper               | (2022) 
| [edzobb@ResearchGate][researchgate]           | 

[IEEE]:https://ieeexplore.ieee.org/document/9610673
[researchgate]: https://www.researchgate.net/profile/Edzo_Botjes2
[doi-thesis]: https://doi.org/10.5281/zenodo.3719388
[git-thesis]: https://gitlab.com/edzob/antifragile-research
[git-article]: https://gitlab.com/edzob/antifragile-article-eaal
[SL-blogger]: https://blog.edzob.com/2019/01/sogeti-labs-posts.html
[AG-blogger]: https://blog.edzob.com/2020/08/ag-connect-blog-posts-dutch.html



## Code sharing
| Code             |        
| -------------    | -------------                 
| [Gitlab][Gitlab] | [Github][Github]   

[Gitlab]: https://gitlab.com/edzob
[Github]: https://github.com/edzob

[pvib-pdf]: https://www.pvib.nl/actueel/ib-magazines/ib-magazine-2021-2/downloaden
[pvib]: https://www.pvib.nl

## Presentations

### Slidedecks
My (public) **Presentation Decks** are on my [Presentation][Public-Slides] page 
and also all on [SlideShare][Slideshare].
Some are recorded.

### Recordings
---

Not all recordings are (yet) retrieved.
Slidedecks of the presentations below are available via the presentations page.

| **Presentation Recordings (mostly) on Youtube**                                          | 
| -------------            | -------------  
| Kader42 |
| PyGrunn 2024  |
| [How to deal with the inevitable chaos of the cloud: on humans and reality.][cyberstorm:2023] | 2023&nbsp;10&nbsp;24 ([youtube](https://www.youtube.com/watch?v=Og7IKtn2YiY), [pdf](https://2023.swisscyberstorm.com/2023/10/24/1435_Edzo_Botjes.pdf), [Google slides](https://docs.google.com/presentation/d/1446sKTposh29mIbRNSlawQ2-Vv6dTdUTWXhHpR-WdB4/edit?usp=sharing))  
| [Optimize Resilience Towards Antifragility, A Secure Cloud Translation by Edzo Botjes@ OWASP BeNeLux 2021][owasp:benelux] | 2022&nbsp;04&nbsp;01 ([youtube](https://www.youtube.com/watch?v=FSFBU0cJ1dU), [Google slides](https://docs.google.com/presentation/d/1mly4yJImBqzuAmbFmbndeRcIVNp6KtYyRv78UY4kulI/edit?usp=sharing))
| [Agility, Resilience, and Antifragility in Practice - Panel (Nov 7) @ the Agility, Resilience, and Antifragility 2022 Virtual Conference][si:webinar]              | 2022&nbsp;11&nbsp;07 
|  Defining Antifragility & the application on Organisation Design @ DADD   | 2021&nbsp;11&nbsp;05
| [Cloud Security - I ain’t rocket science @ Club Cloud][club:cloud]        | 2021&nbsp;11&nbsp;03
| [Attributes relevant to antifragile organizations @ IEEE CBI][CBI:2021]   | 2021&nbsp;09&nbsp;02
| [Value from resilience xebia webinar @ Xebia][xebia:webinar]              | 2021&nbsp;07&nbsp;16 
| Pitch workshop @ AI Hack COVID by AIMED                                   | 2021&nbsp;06&nbsp;23
| [Master in Enterprise IT Architecture through the eyes of one of our alumni @ AMS][ams:2021] |2021&nbsp;05&nbsp;11
| Weerbaarheid in je organisatieontwerp @ SURF                              | 2021&nbsp;04&nbsp;21
| Viable Systems Model @ Xebia XKE                                          | 2021&nbsp;01&nbsp;19
| [Resilience en Antifragility uitgelegd en toegepast @ KNVI][knvi:2020]    | 2020&nbsp;10&nbsp;06
| [DYA Conference - Sensemaking Architecture][dya:dag-2020]                 | 2020&nbsp;09&nbsp;04
| [DYA TechWorkout - Agile and Architecture][dya:tech-workout]              | 2020&nbsp;06&nbsp;29
| [DYA Webinar - Introduction Sensemaking Architecture][dya:webinar-april]  | 2020&nbsp;04&nbsp;17  
| [Intro Why-oh-why @ i&i conferentie 2012][i&i:2012]                       | 2012&nbsp;11&nbsp;08
| [Meet SogetiLabs][sogetilabs:meet]                                        | 2020&nbsp;03&nbsp;04

[cyberstorm:2023]: https://www.youtube.com/watch?v=Og7IKtn2YiY
[owasp:benelux]: https://www.youtube.com/watch?v=FSFBU0cJ1dU
[Slideshare]: https://www.slideshare.net/edzob
[Public-Slides]: https://www.edzob.com/page/slideshare/
[dya:tech-workout]: https://www.sogeti.nl/events/webinar/tech-workout-29-juni-3-juli
[dya:webinar-april]: https://www.sogeti.nl/events/transformational/dya-dag-webinar-2020
[dya:dag-2020]: https://youtu.be/akBA5kFsn4k
[knvi:2020]: https://youtu.be/7gZAGkWTpYY
[club:cloud]: https://youtu.be/70D-QztvzYw
[CBI:2021]: https://youtu.be/Sw5GbSTXIVk
[xebia:webinar]: https://youtu.be/5eaJtcNQPX4
[ams:2021]: https://youtu.be/kRpG2nFwLDM
[i&i:2012]: https://youtu.be/iDGU2moQhhs
[sogetilabs:meet]: https://youtu.be/kN4FWkz0hwE
[si:webinar]: https://www.youtube.com/watch?v=00YbE5mBnC0&list=PLvLXuHY3DBg88WTXrqosKyu9RL6eeETus&index=13

## Podcasts
* 2021-01-13&nbsp;[Software crafts Podcast episode #31](https://www.softwarecraftspodcast.com/episodes/20210113-ep31-edzo-botjes/) on Design for constant evolution by [João Rosa](https://www.twitter.com/joaoasrosa)
* 2020-09-16&nbsp;[Sogeti Technology Leads Podcast episode DYA-Dag](https://anchor.fm/technology-leads-podcast/episodes/DYA-dag-Gasten-Edzo-Botjes--Hans-Nouwens--Marlies-van-Steenbergen--Ton-Eusterbrock-ejm8n4)
by  Tom van de Ven, Rik Marselis en Daniel Laskewitz with as guests  Edzo Botjes, Hans Nouwens, Marlies van Steenbergen & Ton Eusterbrock. 

## Websites
These are some experiments
* [edzob.com](https://www.edzob.com/)
* [DYA BoK - MkDowcs via gitlab wiki](https://edzob.gitlab.io/dya-website-body-of-knowledge/)
* [DYA BoK - HUGO via MD files](https://wiki.dyamm.nl/)
* [Pronto Lectures](https://www.pronto-lectures.com)
* [Master thesis antifragile wiki](https://gitlab.com/edzob/antifragile-research/-/wikis/home)
* ?
