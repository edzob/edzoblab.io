---
title: HTPC - SFF
subtitle: Home Theater PC - Small Form Factor
comments: false
---

# Requirements

## Usage
1. Steam for games that are at least 4 years old.
1. Starcraft 2 with Wine
1. Video-on-Demand (VOD) via Netflix, Disney+, Prime, NPO Start and Youtube on 1040p+.
1. Chrome, Firefox
1. Docker

## Connectivity
1. Gigabit Ethernet.
1. Bluetooth for BT mouse and BT headphones.
1. minimal 2 USB 3.1+ ports 
    1. External HDD, 
    1. External Logitech keyboard.
1. HDMI,  The [x300][x300] supports HDMI(4K @60Hz)

## OS
1. Ubuntu 20.04+

## Casing requirements
1. Low power
1. Low Noice
1. Small Form Factor
1. VESA mount 

## CPU
### AMD over Intel
1. A SFF machine with low power consumption works best with an CPU of the APU type
1. APU is the acronym for integrated Video
1. AMD APU with the AMD Vega is best over Intel integrated Video and AMD Readon integrated video.

## AMD processor type: 4000 with Zen3 over Zen2
1. For 4K 60 Fps 6 cores and 12 threads are minimal
1. AMD 4000 series is better then the 3000 series for the increased cores (6+) and threads (12+) 
    1. ___Note___ Ryzen 3 has 4 cores, Ryzen 5 has 6 cores, and Ryzen 7 has 8 cores.
1. AMD 4000 range contains Zen2 and Zen3 architecture
1. Zen 3 delivers more power output with less power consumption
1. Question: 4000 vs 5000 range
1. [AMD Renoir][WikiChip-renoir] is the 4000 range of AMD of the U / H / HS type. 
These are optimized for low power (15w/35w/45w tdp)

[reddit-r5]: https://www.reddit.com/r/Amd/comments/jktu0t/renoir_apu_idle_power_consumption_improvements/

### AMD Ryzen - renoir Architecture background
1. [Wikipedia - AMD Ryzen][Wikipedia-Ryzen]
    1. [Wikipedia - List of all AMD Ryzen CPU's][Wikipedia-Ryzen-CPU]
    1. [Wikiepdia - List of all AMD Renoir APU CPU's][Wikipedia-APU]
1. [WikiChip - AMD renoir][WikiChip-renoir]
    1. [Tweakers on AMD Ryzen renoir][Tweakers-renoir]
1. [Notebook Check - AMD Renoir Architecture: Ryzen 4000 APUs with Zen 2 and Vega Graphics in 7nm][low-power-07]

[Wikipedia-Ryzen]: https://en.wikipedia.org/wiki/Ryzen
[Wikipedia-Ryzen-CPU]: https://en.wikipedia.org/wiki/List_of_AMD_Ryzen_processors
[Wikipedia-APU]: https://en.wikipedia.org/wiki/List_of_AMD_accelerated_processing_units#%22Renoir%22_(2020)

[WikiChip-renoir]: https://en.wikichip.org/wiki/amd/cores/renoir
[Tweakers-renoir]: https://tweakers.net/nieuws/169984/amd-introduceert-ryzen-4000g-serie-apus-voor-systeembouwers.html
[low-power-07]: https://www.notebookcheck.net/AMD-Renoir-Architecture-7nm-Ryzen-4000-APUs-with-Zen-2-and-Vega-GPU.449815.0.html

#### to renoir or not to renoir
according to [Wikichip][WikiChip-renoir] - " Renoir is codename for AMD series of mainstream mobile and desktop APUs based on the Zen 2 CPU and Vega GPU microarchitectures succeeding Picasso. Renoir processors are fabricated on TSMC 7 nm process. ... Renoir is an SoC for the mobile segment and mainstream desktop and workstation based on the Zen 2 microarchitecture incorporating a Vega GPU. " 

according to [Wikipedia][Wikipedia-Ryzen] - " The Ryzen 4000 APUs are based on Renoir, a refresh of the Zen 2 Matisse CPU cores, coupled with Radeon Vega GPU cores. They were released only to OEM manufacturers in mid-2020" 

### Low Power CPU 
1. German lowpower CPU list states that AMD Ryzen 3 PRO 4350G 
is very nice on power consumption, but not that many core/threads.
1. [Reddit][reddit-r5] post on Ryzen 5 4650G is very nice
1. [hardwareluxx - Die sparsamsten Systeme (<30W Idle)][low-power-01], German forum post on low power
    1. [hardwareluxx - Die sparsamsten Systeme (<30W Idle) Sheet][low-power-02], Google Spreadsheet on low power 
1. [Tweakers - Het grote zuinige server topic - deel 2][low-power-03]
1. [CPU Benchmark - power to performance ratio][low-power-06]

[low-power-01]: https://www.hardwareluxx.de/community/threads/die-sparsamsten-systeme-30w-idle.1007101/
[low-power-02]: https://goo.gl/z8nt3A
[low-power-03]: https://gathering.tweakers.net/forum/list_messages/1673583/340
[low-power-06]: https://www.cpubenchmark.net/power_performance.html

## Casing type: ARock Deskmini X300 or 4x4
1. [Asrock deskmini X300][x300] supports AMD Renoir (4000 series) up to 65W and the AM4 socket
1. Asrock deskmini is SFF, silent and low power consumption
1. The X300 is around [155 EUR][x300-price] or [149 EUR][x300-price2].

[x300]: https://www.asrock.com/nettop/AMD/DeskMini%20X300%20Series/index.asp

## What is the Deskmini X300 ? 
1. [Toms Hardware Guide - New ASRock DeskMini Mini PCs Come in AMD Ryzen 4000, Intel 10th Gen Flavors
][X300-tom] 
1. [Notebook Check - ASRock DeskMini X300 and A300 mini PC barebones first to support up to an AMD Ryzen 7 Pro 4750G APU][X300-notebook] 

[X300-tom]: https://www.tomshardware.com/news/new-asrock-deskmini-mini-pcs-come-in-amd-ryzen-4000-intel-10th-gen-flavors
[X300-notebook]: https://www.notebookcheck.net/ASRock-DeskMini-X300-and-A300-mini-PC-barebones-first-to-support-up-to-an-AMD-Ryzen-7-Pro-4750G-APU.492304.0.html

### AMD Chips supported by the X300 (price excluded CPU)
This is a sub-set from the site of [ASRock][x300-cpu] selected based on 4000 series (renoir)
Note, according to [Wikipedia][Wikipedia-Ryzen] - " Ryzen Pro 4x50G APUs are the same as 4x00G APUs, except they are bundled a Wraith Stealth cooler and are not OEM-only." Thus CPU's below are including Cooler, since the have +50 in the name. The *G is max TDP 65 Watt and the *GE is max TDP 15 Watt. 

* Ryzen 7 PRO [4750G][cpu-monkey-4750] (100-000000145) (155 EUR + [~400 EUR][4750G] = 555)
* Ryzen 7 PRO 4750GE(100-000000152)
* Ryzen 5 PRO [4650G][cpu-mokey-4650] (100-000000143) (155 + [~255 EUR][4650G] = 410)
* Ryzen 5 PRO 4650GE(100-000000153)
* Ryzen 3 PRO [4350G][cpu-monkey-4350] (100-000000148) (155 + [170 EUR][4350G] = 325)
* Ryzen 3 PRO 4350GE(100-000000154)

[4750G]: https://tweakers.net/pricewatch/1579006/amd-ryzen-7-pro-4750g-tray.html
[4650G]: https://tweakers.net/pricewatch/1609284/amd-ryzen-5-pro-4650g-boxed.html
[4350G]: https://tweakers.net/pricewatch/1631442/amd-ryzen-3-pro-4350g-tray.html

[cpu-monkey-4750]: https://www.cpu-monkey.com/en/cpu-amd_ryzen_7_pro_4750g-1560
[cpu-monkey-4650]: https://www.cpu-monkey.com/en/cpu-amd_ryzen_5_pro_4650g-1630
[cpu-monkey-4350]: https://www.cpu-monkey.com/en/cpu-amd_ryzen_3_pro_4350g-1631

#### Comparison
1. CPU-Monkey: [AMD Ryzen 5 3400G vs AMD Ryzen 5 Pro 4650G][cpu-monkey-5vs5]
1. CPU-Benchmark: [AMD Ryzen 5 3400G vs AMD Ryzen 5 Pro 4650G][cpu-benchmark-5v5]

[cpu-monkey-5vs5]: https://www.cpu-monkey.com/en/compare_cpu-amd_ryzen_5_3400g-953-vs-amd_ryzen_5_pro_4650g-1630
[cpu-benchmark-5v5]: https://www.cpubenchmark.net/compare/AMD-Ryzen-5-3400G-vs-AMD-Ryzen-5-PRO-4650G/3498vs3795

### AMD Chips supported by the Asus Mini PC (price include CPU)
* Ryzen 7 4800U - (~650 EUR)
* Ryzen 7 4700U - (~500 EUR)
* Ryzen 5 4600U - ()
* Ryzen 5 4500U - (~450 EUR)
* Ryzen 3 4300U - (~410 EUR)

### ASRock 4x4 Box with 400U range CPU included
Not available in NL, and more pricy then the ASUS and X300
See for a review: 
[Toms Hardware Guide - Hands-on With ASRock’s NUC-Like 4X4 BOX-4800U Mini PC](https://www.tomshardware.com/news/asrock-4x4-box-4800u-hands-on)

[x300-cpu]: https://www.asrock.com/nettop/AMD/DeskMini%20X300%20Series/index.asp#CPU

### Alternative cases
These cases are inclused with the CPU,
1. the 4x4 Boks is arond [599 USD][4x4-price].
1. ASRock industrial [4x4][4x4] supports AMD 4000u series and SSD + HDD combination.
1. Asus Mini PC PN50-BBR545MD-CSM [400 EUR][Asus-price] incl AMD Ryzen 5 4500U

[4x4]: https://www.asrockind.com/4X4%20BOX-4800U
[4x4-price]: https://www.tomshardware.com/news/asrock-4x4-box-4800u-hands-on
[x300-price]: https://minipc.eu/computers/zelf-samenstellen/micro-pc/asrock-deskmini-x300
[x300-price2]: https://tweakers.net/pricewatch/1594026/asrock-deskmini-x300/specificaties/
[Asus-price]: https://tweakers.net/pricewatch/1589698/asus-asus-mini-pc-pn50-bbr545md-csm.html

## Memory
1. More and Faster RAM is always best to keep a system running for 7+ years.
1. two banks are needed, to provide the CPU with "dual" channel access
1. Size: min 32GB -> 2x 16 GB
1. speed: 3200 MHz
1. type: DDR4 (SODIMM) 
1. other spec: 

The [x300][x300] supports 2 x SO-DIMM DDR4 Memory, Max. 64GB, AMD Ryzen 4000 series – 3200MHz

## Harddisk
1. 1 SDD 500GB - 1 TB for OS (maybe even 250GB?, high speed!), 
1. 1 HDD 5TB fast for iso's, DMG, Steam, SC2 etc

SDD: The [x300][x300] supports 
- 1 x Ultra M.2 (2280) PCIe Gen3 x4 SSD Slot
- 1 x Ultra M.2 (2280) Slot
- PCIe Gen3 x4 (Renoir, Picasso and Raven Ridge APU)

HDD: The [x300][x300] supports 
2 x 2.5" SATA 6Gb Hard Drive


# Related Links - Build Request for Review on Tweakers.net
1. My question on Tweakers on Low Power (apr 2020), [Tweakers - Idle power consumption - waar kan ik me inlezen?][my-question-02]
1. [Tweakers - Asrok deskmini build][my-question-03] compariable to my orginal post.
1. [Tweakers - HTPC - low idle power, 4K - compatible componenten?][my-question-01], 
My build review request on Tweakers (jun 2020)
1. My [youtube playlist][yt] on HTPC-SFF focused on the X300 Asrock mini case

[yt]: https://youtube.com/playlist?list=PLOo206xZz3BWAbblXOvQTGrNQ-VXk0X5O
[my-question-01]: https://gathering.tweakers.net/forum/list_messages/1997990
[my-question-02]: https://gathering.tweakers.net/forum/list_messages/1997542/0
[my-question-03]: https://gathering.tweakers.net/forum/list_message/62499310#62499310

# Related Links

## Related Links with overal CPU's benchmarks and comparisons

1. [Notebook Check - CPU Benchmark - Comparison of Mobile Processors][low-power-04]
1. [Tom's Hardware Guide - CPU Benchmark][low-power-05]

[low-power-04]: https://www.notebookcheck.net/Mobile-Processors-Benchmark-List.2436.0.html
[low-power-05]: https://www.tomshardware.com/reviews/cpu-hierarchy,4312.html

## Resources
1. [PartPicker][kit-01], great site to create a bill of materials and build
1. [CPU Monkey](https://www.cpu-monkey.com/)
1. [Wiki Chip](https://en.wikichip.org/wiki/WikiChip)
1. [Notebookcheck](https://www.notebookcheck.net/)
1. [Tom's Hardware Guide](https://www.tomshardware.com)
1. [Guru 3D](https://www.guru3d.com/)
1. [CPU Benchmark](https://www.cpubenchmark.net/)

[kit-01]: https://de.pcpartpicker.com/

