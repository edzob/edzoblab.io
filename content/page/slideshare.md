---
title: Presentations
subtitle: most available on SlideShare, some on Google Presentations or Zenodo.
comments: false
---

This is a very long list. 
## 2025 - SVB
2025-02-10 
[Google Presentation](https://docs.google.com/presentation/d/1XXuqJkZykMVoSd6A2hCH7jdYR8PlIhGy2pn_EW2kvig)


## 2025 - Guest Lecture UU
2025-01-16 [Google Presentation](https://docs.google.com/presentation/d/169t5jWT8AWDWWEpmrjfB_WXRAeURwa4DwnONFU_LovU)

## 2024 - AiGrunn - Contextual AI - JP van Oosten - E.A. Botjes  
2024-11-29
[Google Presentation](https://docs.google.com/presentation/d/1JUS2UqKY_D0wuQaY15_mESbkc6scGnLFYYjsqy7z2to){{< rawhtml >}}<br />{{</ rawhtml >}}
[Youtube Recording](https://www.youtube.com/watch?v=cCcBX2vLDas)

## 2024 - Eraneos 
2024-11-26
[Google Presentation](https://docs.google.com/presentation/d/1uL8dFRj_8COapTES3sSpV0Llci1iLR16JncZKWVIXvQ)

## 2024 - Promis-ES - Creating business value from cyber resilience. Moving towards antifragility through organizational learning.
2024-11-23
[Google Presentation](https://docs.google.com/presentation/d/17pAQzb_dTY7StQOuLgf9hsaL91opKEb30_AVas-RDjM)


## 2024 - Sensemakers - Risk, Resilience & antifragility. Securing your team, solution and company to embrace chaos
2024-11-20
[Google Presentation](https://docs.google.com/presentation/d/1AdYEnYex0td50XCUqKHXv-MxInoo7piHFbhI-Pkbuwg)

## 2024 - Risk & Resilience Festival uTwente - Emotion Regulation
2024-11-07
[Google Presentation](https://docs.google.com/presentation/d/17Mo0OImHiexM-_sdu75m-mtLdHewbblOol7UGIujqK8)

## 2024 - Kader42 - Antifragility and the Art of Embracing Chaos
2024-10-16
[Google Presentation](https://docs.google.com/presentation/d/117fJjQm19ww-5T0A1VPoKgBgh9V3iO-zBnEk_ci9vAc){{< rawhtml >}}<br />{{</ rawhtml >}}
[Youtube Recording](https://www.youtube.com/watch?v=fhsHc9AJqi4)

## 2024 - NWB   - Risk, Resilience & antifragility - Securing  your team, solution and company to embrace chaos
2024-09-05
[Google Presentation](https://docs.google.com/presentation/d/1WN_rDgXEi2htiHV5rbgpXT4Dsg_NeFpvXTj7fYcoo7E)

## 2024 - SSC-ICT - Secure Cloud Journey
2024-09-05 
[Google Presentation](https://docs.google.com/presentation/d/1h_RZabJPoKfKq4nfc8i3EnsqbIxlUMz2N9X4RTFXcOE)

## 2024 - Guest Lecture HU & AMS - Data Architecture, Data Integration, Data Deployment
2024-08-26
[Google Presentation](https://docs.google.com/presentation/d/1-NMfDCGAIokKYjx4QGJIFzF7I6vDyQtIUkZLzLCRM9s)

## 2024 - Guest Lecture UU - Enterprise Architecture & The future world
2024-06-18
[Google Presentation](https://docs.google.com/presentation/d/1mi3wXH0yiC7ESlnj5srIZFQcZqUga0_WOxOH-RFdlsE)

## 2024 - Guest Lecture HU - Introduction into Enterprise Architecture
2024-06-19
[Google Presentation](https://docs.google.com/presentation/d/1MwOrRHhafg4oH8JSIUXCyFKFl1SQ5NU1LpdwMS70Kn0)

## 2024 - OU & Route443 
2024-06-06

## 2024 - PyGrunn Securing your team, solution and company to embrace chaos
2024-05-16
[Google Presentation](https://docs.google.com/presentation/d/1j7HgfiZXd51QdPHD1_yptzbxx8s9O2BNsA0dCG-Ajes){{< rawhtml >}}<br />{{</ rawhtml >}}
[Youtube Recording](https://www.youtube.com/watch?v=VoYk6Gi3NvI)

## 2023 - Swiss Cyber Storm Conference - How to deal with the inevitable chaos of the cloud. A story on humans and reality.
2023-10-24 
[Google Presentation](https://docs.google.com/presentation/d/1446sKTposh29mIbRNSlawQ2-Vv6dTdUTWXhHpR-WdB4){{< rawhtml >}}<br />{{</ rawhtml >}}
[Youtube Recording](https://www.youtube.com/watch?v=Og7IKtn2YiY&t=1s){{< rawhtml >}}<br />{{</ rawhtml >}}
[Conference site with schedule, slides and recording](https://2023.swisscyberstorm.com/schedule/)

{{< rawhtml >}}
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTqkxeAds8HmQY-eVCn5BKFOPhWmy9qxJbsykTcuN3K71Y_HU5QhQ4hAQXjdOMJLmCWpwmdVc0P5RhS/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

## 2023 - Wicca & Xebia - Security, Enterprise Architecture, Software Engineering and  Language Philosophy - Why this is a great marriage 
2023-07-26 
[Google Presentation](https://docs.google.com/presentation/d/10ani0DhNcq1b0O09TldSZSQEjC-poyHmsAWoTvd9SWM)

---
## 2023 - Xebia XKE - Resilience Refresher. What is resilience and how to achieve it.
2023-05-02
[Google Presentation](https://docs.google.com/presentation/d/1lPJg2y6UKPATnlSbuvmsxInJpXHNLtG9KcCVj7eTL7I)
with some strange render issue.. to be looked into.

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSg-yDyaTRbFbVm-J4ml8m8YVeF9TyXJ60g7un60IiBXjyx44aUn6DvxW5XA55d0dc9k2OpbbH2wrzP/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}

---
## 2023 - 62nd ESReDA Seminar UTwente - Embrace chaos and antifragility
2023-04-12  
[Google Presentation](https://docs.google.com/presentation/d/1FVbEETxY5uhzwHJJk_KOm_wWkTL-_eSaJGldpRcjuTA)
by Marinus J. Kuivenhoven and Edzo A. Botjes

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR-ieIKFPI0qFZZRgPHs1f-4KJ2DJ34uKfuJvPFNZDmO-xCrpcqUuGWlthW2iIsDgxX-QTvgcThf4yX/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}

---
## 2023 - NS Conference - Reliability, Resiliency, Antifragility. Engineer for the knowable - prepare for the unknowable.
2023-02-16 
[Google Presentation](https://docs.google.com/presentation/d/1D_cp7LICu1vLLaBsX6i-hHurC6JNwAvTDFLjCfTu-jI)
by Balázs Nagy and Edzo Botjes 

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ0HNkjfZkM-LhC3n6TVIzPHW6i3Jm1ijt6wguhL3BnZ-9RpN0GFw4yPoez0OZ6HaMp61lSKMyYnbDg/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}

---
## 2023 - Xebia - May the 4th minus 1
2023-05-03
[Google Presentation](https://docs.google.com/presentation/d/1IQGjLB1f61nJyXvSHyuTj_tDzFqtiRqJLJxOP9dd-VA/edit?usp=sharing)

{{< rawhtml >}}
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSXxuOHg1aqfI7gFWtti_rd9YdLO0rl_1Whxq5ulUhxN26-jbF-w3GDsknmX5Ky9wg0sgQdMnMaGk_n/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

---
## 2022 - The Risk and Resilience festival UTwente - Embrace chaos and antifragility
2022-11-10 
[Google Presentation](https://docs.google.com/presentation/d/1R5Bh6nNE6hwYkKQJeffWYLY0yNJE-TokrvW8ni9ZY9k/edit?usp=sharing)
festival - by Marinus J. Kuivenhoven and Edzo A. Botjes 

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQW5GRoNXkBBuqMIEGcoCgO3OvccxCv5HmYvvtaJUg6Wyg7rw-TkL7gUr8Jy0ddU4CNe5hVks74aPdM/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}

---
## 2022 - PROMIS OU - Embrace chaos and gain value by continuous learning
2022-11-05 
[Google Presentation](https://docs.google.com/presentation/d/1jDvr1lwypxAdiLLvWS2kDqkt_QH63PHtOKtXEFuiDHc/edit?usp=sharing)

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQnr0LhkwNufsknbIsuhx1-cb9qG3yX5eBm41G68-J7hVYJbXKjV23BTdsmeuAORaryQmI7lFEmvCK4/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}

---
## 2022 - Antwerp Management School - Guest Lecture - Situational design 
2022-10-06
[Google Presentation](https://docs.google.com/presentation/d/1SDYBJ-w0UhahDsIVWIAGURym3xaOEdLIEZ9shD7MHKI/edit?usp=sharing)

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRgmCKcOlObLuiLDWKuJcIqWet5sdPN5uKOG8eQWn4ptzHLj_FGrhrOn-w-nqCiH4C-46xLzNjeAD80/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}

---
## 2022 - Nyenrode Business University - Guest Lecture - Introduction into Security
2022-05-23
[Google Presentation](https://docs.google.com/presentation/d/1Q47pV4JogJcSCNblfQZ6tImYIQtLBWClZKUT3Zd2BTc)

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQSyFaGuw0TWw9kmEuNr4fCO3G63kBlPymdfjKOpJSasdaxJEEBsqFPzrVO5jTyWo6yotdZ8ue950Sh/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}

([Presentation at Zenodo (pdf)](https://zenodo.org/record/6580724))

---
## 2022 - Xebia XKE - Resilience (1) - Chaos (2) - Learning organisation (3)
2022-12-06 
[Google Presentation](https://docs.google.com/presentation/d/1K7DcoZxjrBwOIh9Sa8bW1PAjecmOCuLP9pDJjBulX7w/edit?usp=sharing)
with some strange render issue.. to be looked into.

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ59rggYPOnGnjFV2U3IDpo5CPmWUHudE8fGVjR_B_HwB1cJGz5ztQ-J1Bwav7pqqi0MmeCHKkMrjPT/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}

---
## 2022 -  OWASP Benelux - Optimizing resilience towards antifragility a secure cloud translation
2022-04-01
[Google Presentation](https://docs.google.com/presentation/d/1mly4yJImBqzuAmbFmbndeRcIVNp6KtYyRv78UY4kulI/edit?usp=sharing){{< rawhtml >}}<br />{{</ rawhtml >}}
[Youtube Recording](https://www.youtube.com/watch?v=FSFBU0cJ1dU)

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRJJCnH9LCNdgWCnjiG1jXoc2ijwSbclOVPnCsf5kjiJhDBH5Sfi6vWKQyBnB5OYSJ0s-PMN7q4XjK8/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}

---
## 2021 - DADD - Defining Antifragility &  the application on Organisation Design
2021-11-05
[Google Presentation](https://docs.google.com/presentation/d/1i20rfTnt1chRqqRwGO4u2OGjoDoyZK_Amy0De1BFDQ4)

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="//www.slideshare.net/slideshow/embed_code/key/vbHro4k61FwTzo" width="960" height="569" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/defining-antifragility-and-the-application-on-organisation-design-dadd-20111005" title="Defining antifragility and the application on organisation design @ DADD 20111005" target="_blank">Defining antifragility and the application on organisation design @ DADD 20111005</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2021 - Club Cloud - Cloud Security. I ain’t rocket science
2021-11-03
[Google Presentation](https://docs.google.com/presentation/d/1pgdzQ4Eu2eZSbeg0uHWyrN2Onlh3m-S2Teha0H5-2dI)

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="//www.slideshare.net/slideshow/embed_code/key/6xPpkxQooBUFkm" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/cloud-secrurity-it-aint-rocketscience-clubcloud-20211103" title="Cloud secrurity - it ain&#x27;t rocketscience @ Club.cloud 20211103" target="_blank">Cloud secrurity - it ain&#x27;t rocketscience @ Club.cloud 20211103</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2021 - IEEE CBI - Attributes relevant to antifragile organizations 
2021-09-02
[Google Presentation](https://docs.google.com/presentation/d/1_bwhncTpbZDVQ-d5Znkg9GBxpIZaPUL_lpRoMj7Or3c)

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="//www.slideshare.net/slideshow/embed_code/key/qhSaNeertbJ7b2" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/attributes-relevant-to-antifragile-organizations-paper-ieee-cbi-2021-20210902" title="Attributes relevant to antifragile organizations - Paper - IEEE CBI 2021 20210902" target="_blank">Attributes relevant to antifragile organizations - Paper - IEEE CBI 2021 20210902</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2021 - Xebia - webinar - Value from resilience 
2021-07-16
[Google Presentation](https://docs.google.com/presentation/d/1P7pSbdcO49XKsbgx8hKr8T6MrSPtgoj7TG54UD07r9E)

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="//www.slideshare.net/slideshow/embed_code/key/Br67L4GAZg5e0J" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/value-from-resilience-xebia-webinar" title="Value from resilience xebia webinar" target="_blank">Value from resilience xebia webinar</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2021 - AIMed 2021 - pitch workshop - AI hack covid
2021-06-23
[Google Presentation](https://docs.google.com/presentation/d/1Arg3gujgfSoVs6F-cEUaPE6dRPfXJ5lsa2UR1sIjxH8)

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="//www.slideshare.net/slideshow/embed_code/key/xmELxz5wC5NtjG" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/ai-hack-covid-aimed-2021-pitch-workshop-2" title="Ai hack covid - aimed 2021 - pitch workshop (2)" target="_blank">Ai hack covid - aimed 2021 - pitch workshop (2)</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2021 - Surf - Weerbaarheid in je organisatieontwerp
2021-04-21
[Google Presentation](https://docs.google.com/presentation/d/1QFjhVyf0B7M7Ydl1-yXyzDNEgGLQKy1OSqnw5OQfI1k)

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="//www.slideshare.net/slideshow/embed_code/key/JmBBNSXqBEneth" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/weerbaardheid-in-je-organisatieontwerp" title="Weerbaardheid in je organisatieontwerp" target="_blank">Weerbaardheid in je organisatieontwerp</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2021 - Xebia XKE - Viable Systems Model
2021-01-19
[Google Presentation](https://docs.google.com/presentation/d/1U8dbVJaBLwesfgeIYg-ndUzs5KuVNwd-65gqMVvbyvc)

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="//www.slideshare.net/slideshow/embed_code/key/pO6lfsl7dKB26L" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/viable-systems-model-241648147" title="Viable Systems Model" target="_blank">Viable Systems Model</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2020 - Xebia XKE - Defining Antifragility and the application on Organisation Design
2020-09-24
[Google Presentation](https://docs.google.com/presentation/d/17U_Osw_lPTRmDxqEP9EeLEszOghXjmmCHy0Ab7moQgw/edit?usp=sharing)

{{< rawhtml >}}<div class="responsive-wrap">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ66W3IYNo1DdsTX1JIRTFZTFhgQ4ObbCJpBb__IPh5W1-Gp-cmfnkVezjmXzzDdAurMAZC-k18BSnz/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}

---
## 2020 -KNVI - Resilience and antifragility v2
2020-10-06
[Google Presentation](https://docs.google.com/presentation/d/1nuL0N3cqqie-VyWuix-z4lroZrPba7YLYu91kutXgD4)

{{< rawhtml >}}<iframe src="//www.slideshare.net/slideshow/embed_code/key/945xexR7c8tOmB" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/resilience-and-antifragility-v2" title="Resilience and antifragility v2" target="_blank">Resilience and antifragility v2</a> </strong> from <strong><a href="//www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2018 - Security Awareness & Weerbaarheid - Het zal mij toch niet overkomen
2018-03-24
[Google Presentation](https://docs.google.com/presentation/d/1suCbjjWw8Hr2405jzIdy8CKfhXMbH35_mKghh5fKPPE)

{{< rawhtml >}}<iframe src="//www.slideshare.net/slideshow/embed_code/key/hQIrJxEqsykyIm" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/security-awareness-weerbaarheid-het-zal-mij-toch-niet-overkomen" title="Security Awareness &amp; Weerbaarheid - Het zal mij toch niet overkomen" target="_blank">Security Awareness &amp; Weerbaarheid - Het zal mij toch niet overkomen</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2018 - Sogeti - CI CD & Your Organization. What will change? How to guide change. 
2018-06-22
[Google Presentation](https://docs.google.com/presentation/d/1DWoKBNnl4juJih7-TtiQpVo4mT78uioVWtL-y-UQ8hQ)

{{< rawhtml >}}<iframe src="//www.slideshare.net/slideshow/embed_code/key/f6ij30Wngybc08" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/ci-cd-your-organization" title="CI CD &amp; Your Organization" target="_blank">CI CD &amp; Your Organization</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2016 - Ignation - Open source - an origin story to freedom
2016-10-07
[Google Presentation](https://docs.google.com/presentation/d/1FajRCr7f-N2T5wJlAHJPYpjJ2_C2q4ViY8JICdyhDJ4)

{{< rawhtml >}}<iframe src="//www.slideshare.net/slideshow/embed_code/key/8xtKaTYpETNjnX" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/open-source-an-origin-story-to-freedom-66858032" title="Open source an origin story to freedom" target="_blank">Open source an origin story to freedom</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2014 - Sogeti - Big Data a quickstart - Big Data from idea to service provider from a Consulting perspective
2014-05-13
[Google Presentation](https://docs.google.com/presentation/d/1ouovGpVfB01nRKkrJtfEfTxSkBbELLWv/edit?usp=sharing&ouid=101353969993318737907&rtpof=true&sd=true)

{{< rawhtml >}}<iframe src="//www.slideshare.net/slideshow/embed_code/key/2CKaF7s5CU8t1e" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/big-data-introduction-step-2-sogeti-consulting-services-business-technology-20140516-v4" title="Big Data from idea to service provider from a Consulting perspective - a quickstart" target="_blank">Big Data from idea to service provider from a Consulting perspective - a quickstart</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2013 - Sogeti - Big data introduction - Big Data from a Consulting perspective
2013-05-28
[Google Presentation](https://docs.google.com/presentation/d/1A36EwBDacClVeuZ4cHQmNRvOm-qgGW5V/edit?usp=sharing&ouid=101353969993318737907&rtpof=true&sd=true)

{{< rawhtml >}}<iframe src="//www.slideshare.net/slideshow/embed_code/key/DStyoZYJGQfdz" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/big-data-introduction-sogeti-consulting-services-business-technology-20130628-v5" title="Big data introduction - Big Data from a Consulting perspective - Sogeti" target="_blank">Big data introduction - Big Data from a Consulting perspective - Sogeti</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2013 - Graph databases are awesome

{{< rawhtml >}}<iframe src="//www.slideshare.net/slideshow/embed_code/key/rvyJhpv2F5cp7i" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/graph-databases-are-awesome" title="Graph databases are awesome" target="_blank">Graph databases are awesome</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
##  2012 - Why, oh Why - De kracht en toepassing van de waarom vraag

{{< rawhtml >}}<iframe src="//www.slideshare.net/slideshow/embed_code/key/nx8kh7aUnXyqR" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/why-o-why-v8" title="Why o why v8" target="_blank">Why o why v8</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## Topclass 2010 - Laat er licht zijn! Kwaliteitsverbetering door gebruik van openheid
{{< rawhtml >}}<iframe src="//www.slideshare.net/slideshow/embed_code/key/sc93R2cjK2ytTZ" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/top-class-open-up-sept-2010" title="Top class open up - sept 2010" target="_blank">Top class open up - sept 2010</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2009 - Only companies see software as a valuable peronal asset
{{< rawhtml >}}<iframe src="//www.slideshare.net/slideshow/embed_code/key/buKBrUm4NoOoqf" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/software-ownership" title="Software Ownership" target="_blank">Software Ownership</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---
## 2009 - What we can learn from spaghetti saus - What the analyst can learn from spaghetti saus
{{< rawhtml >}}<iframe src="//www.slideshare.net/slideshow/embed_code/key/AjgK0PFDFPrqtv" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/edzob/what-the-analyst-can-learn-from-spaghetti-saus" title="What the analyst can learn from spaghetti saus" target="_blank">What the analyst can learn from spaghetti saus</a> </strong> from <strong><a href="https://www.slideshare.net/edzob" target="_blank">Edzo Botjes</a></strong> </div>{{< /rawhtml >}}

---

## Knowledge Decks

### Main Concepts to design a change proposal. 
List of models needed when designing an change proposal (or Statement of work, SoW) 
([Google Presentation](https://docs.google.com/presentation/d/119QGQ3_Rf3mRDKNJ37iwIOGTmwwt2QD1408yZ4yVLa0/edit?usp=sharing))

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ7vc5ekGGHqZubcdTKr5qHVZLadmkiZAzqyXXsCAbf3TrvDN9QbS3DeUTvzshx6-m0i2Ox0Gpgh06M/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

### SoW Quality Assurance Appendix 
([Google Presentation](https://docs.google.com/presentation/d/1Hk7hoA66mSJ9cNEJy-O_VvRHIDmqZvcctjI4bzrfqWg/edit?usp=sharing))

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vThDtMUONIW4zV_skCeQaG0v1mRVWFXlBaB_RTHf3ZR6m4aN-HOD86HiIO1fKtmvqFy7Pc4ZSM4I2t0/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

### Blog Tips and tricks 
([Google Presentation](https://docs.google.com/presentation/d/1VjlImjbXzRK15fukC2nHHHc3OaWJ3xLTUs8pY8CooHs/edit?usp=sharing))

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vToSx0jLKQVXW3XYE-KyHECyqIxKLWgEvW3ZvpQns6YIJYzn8rKifjRI5SUxqoTnFy4b7sedpN25S-i/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}

### Enterprise Design Cheat Sheet 
Which aspects do you need to cover when running/ designing a business
([PDF](https://zenodo.org/record/5884550))

### MSc and EA Books - in permanent Beta
List of Books 
([Google Presentation](https://docs.google.com/presentation/d/174iGCqDcX5g5BFQB4oKTae6cWi48EXMYG3LH8GtpZig/edit?usp=sharing))

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRkJZt-AM1Qh1iNlE6nvPbxydpwYzUO6GogtszIFpfjSWIM1tiWGIJF6NRHV_RWih8kXbPMnPrAWFSl/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

### DevOps / DevSecOps books 
List of books 
([Google Presentation](https://docs.google.com/presentation/d/1Ik50npE3oVh5_81AA1jf7ThDNpwX1ooYZGlx__WZ-Vw/edit?usp=sharing))

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQwkUgUjdlR3gOynZxr_6Psj3pp6S7dTbX6Xr6Tn9jDE7CG8tZQbtbSbiEvJN7JNYraNLaYCBDCHWjY/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

### Edzos Overview - Quality and Continuity aspects
Loads of models on all things 
([Google Presentation](https://docs.google.com/presentation/d/1fFar3YvUGNq3Taez9Ggc8xdHXMvCuFkvGk_FL3lB7ao/edit?usp=sharing))

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSAJ3GRXDt1svM0kNhGtJ3W8oHgHZfv1fBUxRD89HSoqL5i1ESqJPgv681Tc_PPrUQwgju_bR_9oY6-/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

### PhD Research Edzo Botjes 2021-2027 - 20230131 - Research Topics 
list of concepts that are part of Edzo’s PhD journey 
([Google Presentation](https://docs.google.com/presentation/d/1O3t7XNNz1oyqG68CdBoM6Oo27LZfZ9eUMVpgylsXnwQ/edit?usp=sharing))

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR2hDLQuTw5bDTL-o5M9YnU-m8UIL5hbkj5HUXCSTUZBuPkUqdnKbmp9hmRmwXwSCHu-OH3ILuSkuf6/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

### Threatmodel sildes of Secure Solution Slides 202211
([Google Presentation](https://docs.google.com/presentation/d/1DAP23LARNHk8mKDYoOcLqiZbuLvT9wGKQSbkCKsLq5o/edit?usp=sharing))

{{< rawhtml >}}
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR6DCT_7zheCt6KPjMMtGtvRh0eTkxMQg6Vfdc4yQNHptB43jQJbXFRjJCxvoZZLFWLYkLsbeJDqNjA/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

### Information and Perception
([Google Presentation](https://docs.google.com/presentation/d/1JE0hyhgAqZtyaTw4Kq95XLcVgep1cs6jJut1-uL9Zms/edit?usp=sharing))
{{< rawhtml >}}
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRfBzi1n2edGoU91oPjnV0l21aDFs7DCbsHU5j0yyIfIyaUaVVBXei0Nk03LqBlY6w12yCSTzYzyZLi/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

### Cloud OIS Stack BRACE DYA et al 
([Google Presentation](https://docs.google.com/presentation/d/1mkZByvt9Ymue5QX0t5T0Vl5oVwyX2ORa40Eo6dpkKjo/edit?usp=sharing))
{{< rawhtml >}}
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQabeA8F75qnVSi3ryNaVzBffYUk_rlmqmbLO5QP7zb8MQNbCOF-VVSF0wdgJk8On6OYo4Ph17LTzG7/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

### AMS - MSc Thesis Hacks
([Google Presentation](https://docs.google.com/presentation/d/14urXAsFL9YpULW0zQs8-beR3kZC7Pk5TncI-IYmL_Ro/edit?usp=sharing))
{{< rawhtml >}}
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRIF0H27mjEJ2EbTGYhnOv1RwHoB-jVYmAEimvou9voO6dy6UhIIZ3wlRUKSKvbC-dTEKG6wITrs-kd/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}

---

## IAM Reference model
([Google Presentation](https://docs.google.com/presentation/d/1p6y8o22dwXOPJEdaiP5cWCyQBvgNUai9UsQavW5eA5M/edit?usp=sharing))
{{< rawhtml >}}
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTkRQ2Xssx4fmOT2wLApd9QqOkmvz5X5krGABT2RLpeqnAOPZttVq5sOCKsqdDSQGogQn84oBvgJBKJ/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{< /rawhtml >}}
