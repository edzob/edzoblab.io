---
title: Enterprise Architecture
subtitle: Micro-Blogs, Blogs and publications
comments: false
---


# EA Crash Course for Sales

## 2024 - Guest Lecture UU - Enterprise Architecture & The future world
2024-06-18
https://docs.google.com/presentation/d/1mi3wXH0yiC7ESlnj5srIZFQcZqUga0_WOxOH-RFdlsE/edit?usp=sharing

## 2024 - Guest Lecture HU - Introduction into Enterprise Architecture - 2024-06-19
https://docs.google.com/presentation/d/1MwOrRHhafg4oH8JSIUXCyFKFl1SQ5NU1LpdwMS70Kn0/edit?usp=sharing

##  2022 - EA Crash Course for Sales at Google NL
2022-11-29
([Google Presentation](https://docs.google.com/presentation/d/1uhjUQQnTFaSGXq1XrvCYsbgGsdYUXy-7W6IxiB_A-Zk/edit?usp=sharing))

##  2022 -  Crash Course Enterprise Architecture for Sales
2022-08-16
([Google Presentation](https://docs.google.com/presentation/d/1not8cWMWiUHtmcByLGjpx-pd80qbb4UiS7hUiXFrrNU/edit?usp=sharing))

## 2022 - Crash Course Enterprise Architecture
2022-07
([Google Presentation](https://docs.google.com/presentation/d/1fXbvJobXrMZUGXGS61BBhtnxIAmPskrXgqlzoCOobYQ/edit?usp=sharing))

## 2023 - Crash Course Enterprise Architecture
2022-06-15
([Google Presentation](https://docs.google.com/presentation/d/1LAjB3z8w17kqdU8UVG6Y2uyv3OhUIsa0TDIy4VTshPo/edit?usp=sharing))




## Mental Models on Enterprise Architecture, Cyber Security, Cyber Resilience and Antifragility
Ongoing subset of the superset of QA Models ([Google Presentation](https://docs.google.com/presentation/d/1skSg3xT2S94xskuFO4KaZMOOt3h9ob_ofCmJWI1LLhM/edit?usp=sharing)).

{{< rawhtml >}}<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR20iK9aDRHEH5VhZO9w4M9kT-tVOpJ2LKC5EOOqK66PAnkuYS23AlcDokouEui6oiLI4fYqGzsJhtT/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}

