---
title: Resume
subtitle: achieving resilient Business and IT
comments: false
tags: ["Resume", "CV"]
---

Welcome to my **Resume page**.  


Here you can find my most recent resume.
Thank you for your effort of getting to know me.

Nothing trumps a good (face-to-face) conversation over a cup of coffee (or tea). 
To cross the time between now and our meeting, 
here are a few documents to give you more insight in me as a person.

On my about page is also information on my personality and personal moto's. 


{{< table_of_contents >}}

## My Resume in English

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRdXbIXo4HqzHpXMlcOZ0rGsBE3HtP5v9yd2Fhvi_eNAZxUPTovKlkgaXc3cYBxx4cLqz93yZASw2nP/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}

## My Resume in Dutch

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSTInintKF7GTM-GSCT9uoo7H4_7Hgu36sEp-vYy8BEDoD0jiz83Mz7kMEgnEst3zfRHm0KEwE-Hrv5/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>{{< /rawhtml >}}


## Combining Research, Sharing knowledge and Application in reality
{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTlV1ErYR_mtsYfzZjbQw82ZBrCD5K6vkFZkoG2JNV3OgVYoKNc5lTGv2w6ZA7N6nsUJMOsQYEDF-li/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}

## My Clients since 2005
Also see [LinkedIn][LinkedIn]

[LinkedIn]: https://www.linkedin.com/in/edzob

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSNLB_coDVRE4rUQdmWXRs0m0Bk7lfgCWnF2rFgoq32LPwJx-2rScW3rCiAxNnZGCmlAxZxdYVFk99S/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}

## A personal note

Specialised in translating and guiding execution of strategic visions in complex organisations. Experienced in bridging the gap between strategy and tactical and uniting business and IT.

---
How and where you are raised, are part of the fundament of who you are. I grew up in my father’s car workshop where I spent my time tinkering with various computers supporting him and his competitors for whom computers were new. Here I learned that IT is supportive to the work people do. From watching my father dealing with his customers I learned the importance of good service. 

Good service means the focus is on the needs of the customer. IT solutions do not exist for their own sake, they should ideally be nearly invisible: making life easier and more efficient without confusing its users or creating new problems. This is my view since my 14th, inspired by many sci-fi books, philosophy books and experience as an entrepreneur. 

In my capacity of Antifragility Arhitect, which is a combination of an Enterprise Architect and a Security Architect, I strive to do exactly this. 

In my capacity of Enterprise Architect & Enterprise Engineer I strive to transform existing organisations in such a way that there is room to evolve to an organic symbioses between people and IT. 

In recent years my focus has been on anti-fragility, innovation, digital transformation, blockchain, internet of things, startups.

---
My Personality can be described in many ways amongst:
"Myers–Briggs Type Indicator® ENFP (“Champion, Innovator, Campaigner”) with strong developed ENTJ (“Fieldmarshal / Executive”)
Insights Discovery Profile® 47 Helping Inspirer (Accommodating)"
“Clear Eyes, Full Hearts, Can't Lose.” - Coach Taylor

## My Goal Map

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQD4X9NqChcNa7Q1MEEd2IL3uvP-Usu-c1FymSsl63yaTTl1YpdisKSniYF-YSSXOSHAuXGJpTU4kMS/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}


---
## Dutch Resume v2019
 
{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSa2KiJfTmKDaB4Knqygfupc0cVw3I92snne5N8JHuJ0sMZSi506tRoXERBfZGeOzKxkCYf7M53yjHN/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}

## History of Resume's

Between 2006 and 2020 I was working at Sogeti Netherlands. 
This is a IT Consultancy firm.
I have been assigned to around 30 clients since November 2006, 
and had many versions of my Sogeti resume per year.

Once every 5 years I update my resume for the outside world. 
Since 1993 (12 years old) I have been working (in IT) and therefore have had many versions of my CV.

Almost all versions where lost during a SD card crash of a Raspberry pi.
Available versions of my resume can be found on this page. 

A complete list of all assignments (at Sogeti) can also be found at 
[LinkedIn](https://www.linkedin.com/in/edzob/).
I also aim to add side-quests from before my time at Sogeti (volunteering) 
and talent tracks during my time at Sogeti to my [LinkedIn](https://www.linkedin.com/in/edzob/) profile. 

This page contains links to 
my Google Drive and to 
my [Gitlab Profile Repository](https://gitlab.com/edzob/edzob).


## Backlog

### Todo
- Create a resume in MarkDown.
- Create a resume in LaTeX (a4).

### Done
- Translate Dutch Google Slide to English Google Slide.
- Update the Dutch and English Google Slide.
- Select a different theme for the Google Slide version.
- add Goal map.


## Acknowlegdments
- Google Sheet via Embedded code in raw html
	- [2017 James Tharpe - Adding HTML Comments in Markdown ](https://www.jamestharpe.com/code-comments-markdown/)
	- [2018 Ana Ulin - Simple Shortcode to Insert Raw HTML in Hugo](https://anaulin.org/blog/hugo-raw-html-shortcode/)
	- [2020 joymon - Added short code for gdoc](https://github.com/joymon/site-hugo/commit/e1575ec8303583b687290f7aacd6f7a8716a4868#diff-9a0364b9e99bb480dd25e1f0284c8555)
	- [2018 Cecina Babich Morrow - Adding Google Docs to website](https://babichmorrowc.github.io/post/add-google-doc/)
- PDF via shortcode
	- [2020 - Yiping Su - Embed PDF Shortcode ](https://gist.github.com/yiping-allison/b6d65d996823e9ec783757e05be19a45)
	- [2018 - Leslie Myint - Tips for using the Hugo academic theme](https://lmyint.github.io/post/hugo-academic-tips/#adding-cv)
		- [Github of Leslie her site](https://github.com/lmyint/personal_site)
		- [Github pages of Leslie her site](https://lmyint.github.io/)
- Resume vs CV
	- [2020 Alison Doyle - The Difference Between a Resume and a Curriculum Vitae](https://www.thebalancecareers.com/cv-vs-resume-2058495)
- TOC in MD file
	- [2020 - Arnab Kumar Shil - Hugo: Add Table of Contents Anywhere in Markdown File](https://ruddra.com/hugo-add-toc-anywhere/)
