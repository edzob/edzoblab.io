---
title: Enterprise Design
subtitle: summary of relevant concepts when designing an Enterprise
comments: false
---

## Cheat Sheet
The goal of this Enterprise Design Cheat Sheet is to be a comprehensive summary of steps and concepts needed to create a company from scratch or for an existing company to kickstart an essential transformation. The content applies to start-ups, scale-ups and existing (multi-national) organisations and companies. 

## Open Access
This document is created by me (@edzob) and published under the creative commons BY-SA 4.0 license. The content is a summary of available and open knowledge. Feedback is welcome. 

This document will continuously be updated since it is a summary of available knowledge and that source is fast and infinite. This and new versions are available in PDF via Zenodo https://doi.org/10.5281/zenodo.5844459  (v1) and the link on this page will update in the future accordingly.

The source of this document is on Gitlab as an open repository.
https://gitlab.com/edzob/enterprise-design-cheat-sheet


## Enjoy
I hope you enjoy the content of this document and provide some inspiration to investigate one or more aspects of your transformation. You are welcome to contact me to discuss the content and reflect on it. Best to you. 


{{< rawhtml >}}<div class="responsive-wrap">
<iframe src="https://docs.google.com/document/d/e/2PACX-1vSblK7NWgiOig7sYXxvk1Uk8m_6MvdAmTCjRm-53ctZXzCRkAIhwtjtqM3rwAARQ6MpY9VKMZr3ssq1/pub?embedded=true" width="712" height="960"></iframe></div>{{< /rawhtml >}}