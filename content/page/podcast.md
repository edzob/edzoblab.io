---
title: Video and Audio call
subtitle: How to improve video calls and even record a podcast
comments: false
---

# Tips and Tricks for a great video call

## Discalaimer:
This is information available online. 
The content of this page is licenced under Creative Commons By Attribution (CC BY SA 4.0).

## Index
{{< table_of_contents >}}

## Top selection Youtube movies on sound
- Podcastage: 
10 Tips for Better Sounding Vocal Recordings for Beginners (FAQ Series)
https://www.youtube.com/watch?v=Ty8YLqOmbV4

- Podcastage: 
You're Talking Into Your Microphone Wrong (FAQ Series)
https://www.youtube.com/watch?v=iyQ4nJgGHZk

- Alpha Gaming: 
$32 Microphone vs $100 Microphone vs $2000 Microphone
https://www.youtube.com/watch?v=T0jmY6-Eft8

- EposVox: 
Stream Audio Quality is NOT Hard... Stop scaring away viewers!
https://www.youtube.com/watch?v=4KLCE_REbw4

- Edzo’s playlist on Microphones
https://www.youtube.com/playlist?list=PLOo206xZz3BUPeU2QumdB3Hz1mjhwbOXc

See below a list of great accounts on Youtube 
that provide all sorts of tips.

## 6 key aspects for sound in a video conference
1. The more frequencies in your voice delivers a richer sound, and the less energie it costs your co-participants. 
1. The less echo from your room and the less of surrounding sounds, the less energie it costs your co-participants. 
1. The richer the sound on you, the more comfortable and less energy consuming a video call will be.
1. The richer and scharper the video image the less straining a conversation is on you and your co-participants.
1. There is something on eye contact and showing more than just your face.
1. Do not underestimate the importance of bandwidth from your internet and your computer.

## Bottom line to improve your video experience
1. Get a good external camera.
1. Get a good external microphone.
1. Get a good (over-ear) headphone.
1. Get a good internet connection.
1. Get stuff in your room.


# 3 Sound options when behind a laptop
## The laptop built-in microphone
A standard laptop microphone will pick up all the sound in its surrounding. This is called an omni-directional microphone. By default the microphone in the laptop is either placed next to the camera or just above the middle function keys on the keyboard.

Therefore by default the microphone picks up pretty clearly the keyboard usage and the spinning of the laptop fan. And the microphone will pick up all the sound surrounding the laptop, from spoons in cups to people walking round the table.

This makes the built-in laptop microphone less than optimal for people to listen in without effort. 

## Microphone via Inner-ear headphone
In dutch the inner-ear headphones are called “dopjes”

Most modern headphones that come with your telephone are inner-ears and are equipped with a microphone. Usually the microphone is part of the wire around 20 cm below one of the speakers. 

These headphones have a plug (3.5 millimeter mini-jack connection) consisting of 4 regions divided by 3 rings. 

A headphone without a microphone is equipped with 2 rings (TRS), a headphone with a microphone has 3 rings (TRRS). For more on this you can always count on wikipedia. 
https://en.wikipedia.org/wiki/Phone_connector_(audio)
In the beginning laptops have a separate headphone and separate microphone connector that support two rings (TRS).

Most laptops do not support a microphone via the headset connector. You can check via google if your laptop does support the microphone via the headphone connector.

This is your first moment of pondering and choice:
1. When you love your inner-ears, and they work great with your phone. Then use the video meeting application on your phone. It is possible to use Google Meet, Microsoft Teams, Zoom etc via your phone for audio only. Use your phone for just audio and use your laptop for video, chat and screen sharing.
This is the cheapest solution to improve the microphone audio quality.
1. When you are not happy with the need to login twice, or you are not willing to use your phone or there is another reason to only use your laptop, then the only option is to use a microphone via USB.

## Bluetooth headset -> out of scope
Bottom line: do not use bluetooth headsets for video calls. They experience latency and interference. And also have the need for a battery. For a phone call they can be liberating, or for listening to podcasts or youtube movies during cooking. 

Furthermore a headset will pick up lots of surrounding sounds and therefore deliver less from optimal sound during video calls.

## Microphone via USB

### Headset vs separate microphone.
This is your second moment of pondering and choice:
1. Do you prefer a headset with a small microphone connected directed facing you mouth, or
1. Do you prefer a microphone standing on your desk?

### Headset
The benefit of a headset is that it is an over-ear headphone and reduces the sound of the surrounding. The brands Jabra, JBL and Logitech are the best. A cheap but good alternative is the brand trust.  

Another benefit is that the microphone is directed to your mouth and therefore the microphone only picks up the sound of your voice. 

Some people swear by headsets, since the sound quality is amazing and you never have to think about where you put your microphone on your desk. Just put on your headset and go.
Downside is 
- Not everybody loves the way the headset feels on their head and how it looks.
- Due to the corona pandemic many headsets are sold-out and unavailable
- Headsets are not cheap.

This is a very nice overview of available headsets (in the netherlands)
https://www.coolblue.nl/en/headsets/usb-headsets?sort=lowest-price&page=1
Most are out of stock. Via the site 
[tweakers.net][https://tweakers.net/pricewatch/]
often a webshop can be found that still has some in stock of your desired brand and type.

Advice is to buy Jabra or Logitech since they deliver great sound quality, comfort and are top quality. This applies when you are not a gamer. When you are a gamer then you will have your own ways to find the best headset to use the whole day and top it off in the evening with come gaming.

## Seperate microphone 
This option is my personal favorite, but this is clearly a personal opinion.

A USB microphone has two options for sound sensitivity.
1. Omni-directional pattern
1. Cardioid pattern

Cardioid pattern implies that only the front of the microphone is sensitive for picking ups sound. This enables you to put the laptop (and other sound sources) at the back of your microphone so that the sound of the fan in your laptop and your keyboard will not be picked up. 

This site provides a nice overview of the various patterns. 
https://www.sweetwater.com/insync/studio-microphone-buying-guide 


Choosing a separate microphone over a headset leads to a bit more work. You need to learn how and where you position the microphone. In my opinion this weighs up to the freedom gained in selecting and switching to a preferred headphone. 
During the corona pandemic the USB microphones are still plenty available.
See for dutch webshops the following links to their USB Microphone options.
1. Bax-Shop https://www.bax-shop.nl/usb-microfoons?o=popular&p%5Bmax%5D=100&online_stock%5B%5D=0&online_stock%5B%5D=1 
1. Coolblue
https://www.coolblue.nl/zoeken/producttype:microfoons/type-microfoonaansluiting:usb?query=usb%20microfoon&sorteren=laagste-prijs&pagina=1 


#### Great option 1
A very cheap and pretty good option is the Devine M-mic. This is priced at 31 Euro, and comes with a pretty good tripod for placement on your table. The only downside is that it does not have a headphone connector in the microphone. This would be a bonus.

https://www.bax-shop.nl/grootmembraan-condensatormicrofoons/devine-m-mic-usb-bk-condensatormicrofoon-zwart#productinformatie

#### Great option 2
The Samson Go is also relatively cheap (36 Euro) and widely available. The reviews of this one are very good. The form factor is very small and fits in your bag. Another bonus is that it is equipped with a headphone adapter. It is not equipped with a tripod but a different adapter, the reviews are not conclusive if this is an issue. It supports a bit smaller frequency range as the Devine. 

https://www.bax-shop.nl/usb-microfoon/samson-go-mic-miniatuur-usb-condensator-microfoon 

#### more to spend
When you want to spend a bit more, between the 60 and 100 euro’s there are plenty of super microphones that deliver super quality. In this range there are different designs available. For example specific for gamers or youtube stars (to be). 

### Summary
1. *Get a USB microphone*
Since this connects directly to your laptop.
For most people the other option (an xlr microphone) is too much hassle.
1. *Get a USB microphone with a (super) Cardioid pattern*
To prevent sound pickup other than your voice.

### Great shortlist of USB microphones
This is a short list of some USB microphones based on watching many video’s.
1. Devine M-Mic USB BK (30E, [bax-shop][https://www.bax-shop.nl/grootmembraan-condensatormicrofoons/devine-m-mic-usb-bk-condensatormicrofoon-zwart#productinformatie])
1. Samson GO USB (40E, [bax-shop][https://www.bax-shop.nl/usb-microfoon/samson-go-mic-miniatuur-usb-condensator-microfoon])
1. Razer Seiren Mini (60E, [amazon.de][https://www.amazon.de/dp/B08GP86RTQ], [tweakers.net][https://tweakers.net/pricewatch/1629528/razer-seiren-mini-zwart.html], [youtube review][https://www.youtube.com/watch?v=jJQLRzDZ3mo])
1. SUDOTACK ST- 810 (incl standaard, 64E [amazon.de][https://www.amazon.de/dp/B07SYQCT8P], [youtube review][https://youtu.be/HtjbquBYXbs])
1. Yeti Snowball (65E, 65E, [tweakers][https://tweakers.net/pricewatch/487113/blue-microphones-snowball-ice-wit/reviews/])
1. Trust GXT 252 Emita (complete set, 90E, [tweakers][https://tweakers.net/pricewatch/1128739/trust-gxt-252-emita.html])
1. Tip: Audio Technica ATR2500x-USB (120E, [tweakers][https://tweakers.net/pricewatch/1603418/audio-technica-atr2500x-usb-microphone/specificaties/], [youtube review][https://youtu.be/uBvh8n2YC1U])
Populair 2020: Yeti (133E, [tweakers][https://tweakers.net/pricewatch/285892/blue-microphones-yeti-usb-condensermicrofoon-zilver.html])

# Bonus content : condenser vs dynamic microphones.
When you are looking into microphones you will be confronted with the difference between a Condenser microphone and a dynamic microphone. 
Dynamic microphones are used for singing in a band, it will not pick up the surrounding sounds and sounds from accidentally tapping the stand, but therefore also does provide a more flat sound of your voice.

A Condenser microphone is more sensitive. For it to work you need to put the microphone really close to your mouth. (10-15 cm). The condenser microphone delivers a way richer feel to your voice. This is great for podcasting and audiobooks. 

In my experience this difference is only a factor when looking for an XLR microphone since most USB microphones are dynamic cardioid microphones.

See this great flowchart to determine which pattern and type of microphones is best for your: https://www.bandrewscott.com/findtherightmic 

# Youtube channels


## Accounts with Great content

1. Podcastage - delivers a review of almost every possible microphone. Awesome! https://www.youtube.com/c/Podcastage/about 

1. BoothJunkie - delivers reviews and also sound tips. https://www.youtube.com/c/BoothJunkieVO/about 

1. Curtis Judd - delivers review of almost every possible microphone.
https://www.youtube.com/c/curtisjudd/about 

1. Alpha Gaming - great tips on sound, video, composition. Awesome!
https://www.youtube.com/c/AlphaGamingHouse/about

1. Epos Vox - Great content on light and camera’s
https://www.youtube.com/c/EposVox/about 

## On the room and setting

1. How to Start a Podcast 2020 | Equipment & Guide for Beginners (Audio and Video)
@Indy Mogul
https://www.youtube.com/watch?v=HMITDLMW5EM&t

1. Mastering Composition + Cinematography with Will Smith
D4Darious
https://www.youtube.com/watch?v=cIvGRytmRaw 

1. How to Talk to the Camera!
Daniel Schiffer
https://www.youtube.com/watch?v=nQ2QF5Oa0bk


## HOW TO FILM YOURSELF

1. Peter McKinnon
https://www.youtube.com/watch?v=dEfwjZZeBC4

1. This $130 Video Desk Setup Rocks!
@DSLR Video Shooter
https://www.youtube.com/watch?v=xrWpkZix_sI

1. Every Streaming Purchase You Could Need In 2020
Alpha Gaming
https://www.youtube.com/watch?v=IzUoaNWJeug

1. Gitlab - Documentation on Remote working and your Workplace
https://about.gitlab.com/company/culture/all-remote/workspace/

1. Gitlab - Documenataton on Communicating via Video Calls
https://about.gitlab.com/handbook/communication/ 

1. Gitlab - Documentation on Collaboration via Video Calls
https://about.gitlab.com/company/culture/all-remote/collaboration-and-whiteboarding/ 

## On microphones

1. USB vs XLR Mics EXPLAINED! - Everything You Need To Know
Alpha Gaming
https://www.youtube.com/watch?v=XEpPyTu2Ubk 

See the accounts of Alpha Gaming and Podcastage for advice on Microphones in addition to this document.

## On Camera

1. PICKING A LIVE STREAM CAMERA - literally everything you need to know
@Alpha Gaming
https://www.youtube.com/watch?v=OptCyy25M5Y

1. The BEST Budget Camera for Youtube and Streaming?? Sony a5100 v Canon m200 v Panasonic Lumix G7
@Alpha Gaming
https://www.youtube.com/watch?v=3GPz5WRKQwU 

1. 5 Great Cameras to take your Streaming Quality to the NEXT LEVEL
EposVox
https://www.youtube.com/watch?v=61TcpH7RI_8 

See the accounts of Alpha Gaming and Epos Vox for advice on Cameras in addition to this document.

## On Sound 

1. A voiceover vocal booth in 10 minutes - for free?
@Booth Junkie
https://www.youtube.com/watch?v=5Se381sERrY 

1. Creating a high quality audio podcast over Skype
@Booth Junkie
https://www.youtube.com/watch?v=mjiLTGpZqsQ

1. Playlist on how to create your own silent paneling
@edzob 
https://www.youtube.com/playlist?list=PLOo206xZz3BVHDx3ALNHFUcHkV8T3C4Df 

1. Budget Audio Treatment & Budget Vocal Booth (FAQ Series)
Podcastage
https://www.youtube.com/watch?v=7h84lrQQTl8 

1. 10 Tips for Better Sounding Vocal Recordings for Beginners (FAQ Series)
Podcastage
https://www.youtube.com/watch?v=Ty8YLqOmbV4

1. What Is Acoustic Audio Treatment and What Does it Do?
Podcastage
https://www.youtube.com/watch?v=DSP1l77xFyo

1. 4 STEPS to sound like a PRO on Twitch!!
Alpha Gaming
https://www.youtube.com/watch?v=HYgXFVm3-mk

1. How To Make A USB Mic Sound Like A $2000 Studio Microphone
Alpha Gaming
https://www.youtube.com/watch?v=ToHuInqVEHE&t=416s 

## On Light

1. 10 Video LED Lights $50-$100
DSLR Video Shooter
https://www.youtube.com/watch?v=AcBb-MInwBc 

# Setup Shopping Lists

https://kit.co/edzob/budget-podcast-setup-for-four-persons-in-the-same-room-660-eur

Product target prices (EUR) # Total
ULTRAVOICE XM8500 €20.00  4 €80.00
Samson SAMD5 Desktop Microphone Stand €20.00  4 €80.00
Neewer® 6-pack 6.5FT / 2M XLR-stekker naar XLR-bus  €40.00  1 €40.00
Zoom H6 Black €330.00 1 €330.00
Manfrotto PIXI Tafelstatief €25.00  1 €25.00
Sony MDR-ZX110  €13.00  4 €52.00
Eono Aux Kabel 3,5mm Audio Kabel - 1m €6.00 5 €30.00
Amazon Basics 5-way splitter  €11.00  1 €11.00
Ikea Heat €2.00 3 €6.00
Amazon Basics USB 2.0 Kabel €6.00 1 €6.00
    Total €660.00

https://kit.co/edzob/100-eur-budget-xlr-podcast-recording-setup-1-person      
Product target prices (EUR) # Total
Samson SAQ2UHD Q2U  €70.00  1 €70.00
Sony MDR-ZX110 Headphone  €13.00  1 €13.00
      
Samson SAMD5 Desktop Microphone Stand €20.00  1 €20.00
NeewerMikrofonstände  €22.99  0 €0.00
    Total €103.00

https://kit.co/edzob/100-eur-budget-usb-podcast-recording-setup-1-person      
Product target prices (EUR) # Total
ULTRAVOICE XM8500 €20.00  1 €20.00
Amazon Basics - XLR-Mikrofonkabel | 1,8 m €10.70  1 €10.70
Behringer U-PHORIA UM2  €38.00  1 €38.00
Sony MDR-ZX110 Headphone  €13.00  1 €13.00
      
NeewerMikrofonstände  €22.99  1 €22.99
Samson SAMD5 Desktop Microphone Stand €20.00  0 €0.00
    Total €104.69    

1. https://sip.audio/knowledgebase/build_sip_codec_raspberry_pi.php
Build a SIP Codec with a Raspberry Pi


1. https://www.youtube.com/watch?v=NO_L4i9hab0
Build an IP codec for sip.audio with a Raspberry Pi and a USB microphone

https://www.youtube.com/watch?v=r4GSucH4luo
Full duplex 2 way audio streaming(OPUS codec) on a Raspberry PI using GSTREAMER - LIVE DEMO

Streaming Stereo Audio over the internet with a TCP OPUS RTP Gstreamer AUDIO PIPELINE - LIVE demo
https://www.youtube.com/watch?v=kSjKU6N1Y50

https://www.youtube.com/watch?v=ELgsm97UnVA
Stereo Audio Conferencing FULL MESH p2p node - with a Raspberry Pi & the OPUS VOIP APP - Seren

https://www.youtube.com/watch?v=rLpSCvDyhHI
Build your own OPUS VOIP SFU audio conference server using ZeroTier SDN, Gstreamer & a Raspberry PI


# Opus Codec - Raspberry Pi
So there is a way to connect video in low latency 
from various locations. 
This is called NDI.

1. [Comparing Low Latency Streaming Protocols: SRT, WebRTC, LL-HLS, UDP, TCP, RTMP][https://www.muvi.com/blogs/comparing-low-latency-streaming-protocols.html]

1. [Low Latency Video Streaming: The Complete Guide][https://corp.kaltura.com/blog/low-latency-streaming-guide/]

1. [Ultra low latency video streaming and 10 use cases][https://antmedia.io/ultra-low-latency-video-streaming-use-cases/]

1. [Streaming Protocols: Everything You Need to Know (Update)][https://www.wowza.com/blog/streaming-protocols]

1. [Low Latency Streaming Protocols SRT, WebRTC, LL-HLS, UDP, TCP, RTMP Explained][https://ottverse.com/low-latency-streaming-srt-webrtc-ll-hls-udp-tcp-rtmp/]

1. [SRT vs. NDI for remote video production][https://www.epiphan.com/blog/srt-vs-ndi/]

1.  [Wikipedia: Network Device Interface][https://en.wikipedia.org/wiki/Network_Device_Interface]

So there is also a way to connect audio (only) in low latency 
from various locations.
This is called OPUS and SIP.

1. https://sip.audio/knowledgebase/build_sip_codec_raspberry_pi.php

1. [Youtube: Build an IP codec for sip.audio with a Raspberry Pi and a USB microphone][https://www.youtube.com/watch?v=NO_L4i9hab0]

1. [Youtube: A beginner's guide to Raspberry Pi streaming
][https://www.youtube.com/watch?v=3BUjj2mZ4o0]

1. [Youtube: Full duplex 2 way audio streaming(OPUS codec) on a Raspberry PI using GSTREAMER - LIVE DEMO][https://www.youtube.com/watch?v=r4GSucH4luo]

1. [Youtube: Streaming Stereo Audio over the internet with a TCP OPUS RTP Gstreamer AUDIO PIPELINE - LIVE demo][https://www.youtube.com/watch?v=kSjKU6N1Y50]

1. [Youtube: Stereo Audio Conferencing FULL MESH p2p node - with a Raspberry Pi & the OPUS VOIP APP - Seren][https://www.youtube.com/watch?v=ELgsm97UnVA]

1. [Youtube: Build your own OPUS VOIP SFU audio conference server using ZeroTier SDN, Gstreamer & a Raspberry PI][https://www.youtube.com/watch?v=rLpSCvDyhHI]