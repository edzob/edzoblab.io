---
title: Private Presentations
subtitle: Knowledge-tree and mindmaps that I use to organize my mind
comments: false
---

# A collection of decks
{{< table_of_contents >}}

## Knowledge Mindmap Enterprise Architect

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTb_ym1-w2b6_EmHJpsujIDABOeh6F4vRX1w8ESgHNxi201ldq9YqlTbEAErgf07nuY4EyUAH4zgdbF/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}

## PKI knowledge-tree

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRmS0NNWW-O6MpOFhKp_9cz_vWIsm1HLZ9b2-7nsw0wvE7cWfUzCJlnSw_a70s60gf0RzM2YUulKyt8/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}


## Edzos Overview - Quality and Continuity aspects
A Collection of Patterns. 
Scope of this deck is the complete enterprise, 
from business design to test automation.


{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSAJ3GRXDt1svM0kNhGtJ3W8oHgHZfv1fBUxRD89HSoqL5i1ESqJPgv681Tc_PPrUQwgju_bR_9oY6-/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}

## EGIT - COBIT 2019 - Roadmap for Enterprise Governance

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSkiozsTq2tM3g19v-04-mkzJBZX1o7a600MkRD-24T2VU3Ewq84QerJU0sCIEAB3b-LBDB6Gb8GHvC/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}

## Chaos and Complexity - C-Level deck - Edzo Botjes

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRtHP3uTKiQXINquZp5utDxpgei6uplK05PUiZvaJyGCa5KubB39BZUNnh4etDxa2SXF_CQpoaMcoHO/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" 	height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}

## Architecture Community Presentation - Chaos and Complexity - v2 - Edzo Botjes

{{< rawhtml >}}<div class="responsive-wrap"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTtvASIEH4QW_WFS4ZSoYREeXwVg2j9wr7OggK1VTCfk0q2Cfxh_WZkwTocAls6pt__eP6zqkRwOfYw/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" 	height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></div>{{< /rawhtml >}}


<!-- width="1587" 	height="1152"	--> 
<!-- width="960" 	height="569" 	-->
<!-- width="1280" 	height="749"  	-->
