---
title: The cave or reality
subtitle: A collection of cultural piece
comments: false
---

- What is real and what is not?
- When is something artificial becomes real?
- How do you know the difference?

# Movies
1. Blade Runner
1. Blade Runner 2049
1. The Matrix
1. Existence
1. Her
1. Ex-Machina
1. AI
1. Westworld the series
1. Daryll


# Books


# Blog post - Duality of reality

[Duality of reality](https://edzob.medium.com/duality-of-reality-dfe94b693ae6) Posted on medium 2023-01-09

The duality between the reality outside of us being in continuos flux and the reflection of reality in our own mind being incomplete, is at the core of science and of fiction (ref). Science aims to grasp the reality and predict it, where fiction plays with the incompleteness of your internal reality (ref).

In every field of science, the school of understanding the outside world and the school of acknowledging being caught in our own personal reality are present and exchange places in being the dominant school of thought.

Quality Criteria for science are generally to be considered Replicability, Falsification, Independence and Precision (Recker, 2012, p. 16). Karl Popper famously stated that science is only what can be verified and falsified, and otherwise it is only a doctrine or pseudoscience (Veronesi, 2014). Replication research (Hagger et al., 2016; Open Science Collaboration et al., 2015) has shown that replication has big challenges, resulting in a replication crisis in at least the field of psychology and economy (Ioannidis, 2005; Kaleliolu, 2021).

Postmodernism is a school in (science) philosophy that states that the internal mental model of an individual person has impact on observation, sense making and action. Therefore, the internal mental model of a person defines the reality of that person (see work from Plato, Nietzsche , Kant, Baudrillard, Foucault).

The outside world influences the internal mental model of a person via education and culture (Hestenes, 2006; Jones et al., 2011; Rousmaniere et al., 2017), and therefore the outside world is influencing the behaviour of systems (Jackson, 2019), organisations (Archer, 1995; Hoogervorst, 2017; Stacey, 2009) and individual persons (M. R. Bennett & Hacker, 2022; Karnath & Thier, 2006; McGilchrist, 2021). This can play a role in the forementioned replication crisis.

In the field of critical realism (Gorski, 2013), it is proposed to combine research methods (Edwards et al., 2014; Howell, 2012; Sayer, 1992) to acknowledge that the internal reality of the person and the reality outside the person both influence each other.

A downside of post-modernism in the current Volatile, Uncertain, Complex and Ambiguous world (N. Bennett & Lemoine, 2014a; 2014b) is that describing a concept with high detail is very difficult due to the involved individuals and collection of individuals have their own respective reality that changes at a high pace, in combination to the outside reality changing quickly. This hinders a unified view, coordinated actions and predictable outcome.

To compensate for this in the design of organisations the disciplines of Enterprise Architecture (Ross et al., 2019) and Risk Management (Hutchins, 2018) need to adapt toward accommodating the and accommodating uncertainty.

This text is part of my PhD research: “How Can Organisational Learning Be Leveraged to Enable Antifragility of an Organisation?” documented on Gitlab: https://gitlab.com/edzob/complex_adaptive_systems-knowledge_base/-/wikis/home

Content is by default open source and licenced under Creative Commons BY-SA 4.0
