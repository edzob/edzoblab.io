---
title: Topic - Organisation Resilience
subtitle: An organisation needs to adapt non-stop.
date: 2020-07-25
tags: ["Resilience", "Antifragile", "topic"]
---

Organisations need not only be to designed around a purpose, 
but also according a specific behaviour.

I have written my master thesis in 2020 on the topic of antifragility titled:
"Defining Antifragility and the application on Organisation Design 
- a literature study in the field of antifragility, 
applied in the context of organisation design".

The thesis is available via [Zenodo DOI](https://doi.org/10.5281/zenodo.3719388)
The thesis was rewarded with 19 out of 20 points.

I achieved a score of 86% 
for my Executive Master education 
in [Enterprise IT Architecture](https://www.antwerpmanagementschool.be/en/program/executive-master-enterprise-it-architecture)
at the [Antwerp Management School](https://www.antwerpmanagementschool.be/en/about) (AMS) 
from Sept 27 2018 to June 26, 2020.

This Executive Master included 
Digital Transformation (Strategy & Leadership), 
Enterprise Governance (COBIT 2019), 
Enterprise Engineering (EE, DEMO), 
Normalized Systems Theory and 
Information Security Management, 

The score of 86% earned me the qualification of [summa cum laude](https://nl.wikipedia.org/wiki/Cum_laude#Belgi%C3%AB) 
also known as [with the highest distinctions](https://www.linkedin.com/posts/edzob_antifragile-antifragility-ams-activity-6682566804621037568-dh4B). 

I will be pursuing to do more academic research and 
write more articles on the topic of resilience, antifragility and organisation design.

All my academic work is and will be under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). 
This to make sure that everybody is able to validate and replicate the research.
Also I believe that science that is locked away is not science since it can not be validated and replicated,
and it does not align with my view on open access and open knowledge.

I have not yet found any use case where I could not combine Open with compliancy, security and privacy.