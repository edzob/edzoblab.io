# Welcome!

Hi there! Welcome!

My goal is to make organisations more resilient and most important a human-centred place to work. 
I believe this will lead to less stress and more happiness.

My aim is to achieve this goal via applying my expertise as Antifragility Architect,
Variety Engineer and
Security Coach in and around organisations.
I also believe that sharing as much as possible content via
blogs, articles, lectures and courses helps in achieving this goal.

I currently 
1. help multiple companies with their security challenges, 
1. teach Enterprise Architecture at the Applied Science University of Utrecht and
         Enterprise Security Architecture &amp; Cyber Resilience at the Antwerp Management School
1. do my PhD in Information Security, Cyber Resilience and Organisational Learning at the Open University of the Netherlands, and
1. am active in various knowledge sharing and building initiatives (e.g. KNVI, OWASP, CyberMeister, Leonardo's).


## Contact

📝 on [My Contact](page/contact/) page you can find my socials and other contact details.  
__Note__: For my email and phone number, see [My Resume](page/resume/).

## My services offered to client since 2021

1. Continuous organisational improvement
    1. Improve the security level of your organisation.
    1. Improve the level of resilience of your organisation.
1. Remove strategic bottle-necks
    1. Help a stranded program back on track. 
    1. Achieve the impossible goal.
    1. Second opinion on your architecture.
1. Continuous education
    1. Courses on Resilience, Chaos, Anti-fragile and Enterprise Architecture.
    1. C-Level Coaching.
    1. Personal Coaching.

## Continuous knowledge sharing
To me sharing content is a very natural thing to do. 
We all are standing on shoulders of giants and therefore it is logical to help each other. I am a big fan of open access, open science and open source.

📓 I share [My Content](page/content/) via Twitter, Blogs, Public Presentations and Academic Publications.   
📈 On [SlideShare](page/slideshare/) I share my public presentations.   
🎓 My [Academic Content](page/content/) is index on [ORCID][ORCid] and available via [My Content](page/content/) page.    
👨‍💻 My [Code](page/content/) is available on [GitLab][Gitlab] and [GitHub][Github].  


[ORCid]: https://orcid.org/0000-0003-0097-7375
[Gitlab]: https://gitlab.com/edzob
[Github]: https://github.com/edzob


## My Resume
✔ [My Resume](page/resume/) is available as a presentation and as an A4-document in English and Dutch.   
🎯 My personal goals are continuous updated and on [My Resume](page/resume/) page.  
✔ My Clients and roles since 2005 are also to be found on [My Resume](page/resume/) page.  

---
## Personal notes

💡 Some topic I put into [slide-decks](page/slides/).
These includes PKI, Enterprise Architecture skills and
a collection of a wide variety of existing models. 
Creating these overviews is for me a way to hack my dyslexia.   
🦉These [Topics](https://edzob.gitlab.io/tags/topic/)
play a large role in my view on organisations.   
📚 My collection of [Presentation Decks](page/slides/) 
from my private library, 
filled with information/brain-dumps for example PKI, 
Enterprise Architecture, collection of great "patterns". 


## Acknowledgement 
This website is powered by 
[GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo SSG](https://gohugo.io) 
/ [Beautiful Hugo theme](https://themes.gohugo.io/beautifulhugo/)    
This website is inspired by 
[Emojipedia](https://emojipedia.org)
/ [Make a README](https://www.makeareadme.com/)
/ [Awesome README](https://github.com/matiassingers/awesome-readme)
/ [Awesome Profile README](https://github.com/kautukkundan/Awesome-Profile-README-templates)

{{< youtube w7Ft2ymGmfc >}}

